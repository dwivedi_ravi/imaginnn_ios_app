//
//  BaseViewController.swift
//  Imaginnn
//
//  Created by Smruti Kanta Mohanty on 03/09/21.
//

import UIKit
//AKSwiftSlideMenu
class BaseViewController: UIViewController, SlideMenuDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    func slideMenuItemSelectedAtIndex(_ index: Int32) {
      //  let topViewController : UIViewController = self.navigationController!.topViewController!
     //   print("View Controller is : \(topViewController) \n", terminator: "")
        switch(index){
        case 0:
            
            self.openViewControllerBasedOnIdentifier(SEGUES.changePinSegue.rawValue)

        case 1:
           
            self.openViewControllerBasedOnIdentifier(SEGUES.termsOfUseSegue.rawValue)

        case 2:
        
             self.openViewControllerBasedOnIdentifier(SEGUES.rateTheAppSegue.rawValue)
            
        case 3:

            self.openViewControllerBasedOnIdentifier(SEGUES.contactUsSegue.rawValue)
             
            break
            
        case 4:

            print("Invite Friends")
             
            break
            
        case 5:

            self.openViewControllerBasedOnIdentifier(SEGUES.faqsSegue.rawValue)
             
            break
            
        case 6:

            print("Log Out")
            appDelegate?.logOutUser()
            break
            
        case 101:

            self.openViewControllerBasedOnIdentifier(SEGUES.myProfileSegue.rawValue)
             
            break
            
        
            
        default:
           break
        }
    }
    
    func openViewControllerBasedOnIdentifier(_ strIdentifier:String){
        let destViewController : UIViewController = self.storyboard!.instantiateViewController(withIdentifier: strIdentifier)
        
        /*let topViewController : UIViewController = self.navigationController!.topViewController!
        
        if (topViewController.restorationIdentifier! == destViewController.restorationIdentifier!){
            print("Same VC")
        } else {
            self.navigationController!.pushViewController(destViewController, animated: true)
        } */
        
       // destViewController.modalPresentationStyle = .popover
        
        present(destViewController, animated: true, completion: nil)
        
        /*
        // Present ViewController On present viewcontroller
        self.addChild(destViewController)
        self.view.addSubview(destViewController.view)
        destViewController.view.frame = CGRect(x: 0, y: self.view.frame.height, width: self.view.frame.width, height: self.view.frame.height)
        destViewController.willMove(toParent: self)
        UIView.animate(withDuration: 0.2) {
            destViewController.view.frame = self.view.frame
        } */
        
    }
    
    func addCustomUIButton(_ button: UIButton) {
        button.tag = 101
        button.addTarget(self, action: #selector(BaseViewController.onSlideMenuButtonPressed(_ :)), for: UIControl.Event.touchUpInside)
    }
    
    func addSlideMenuButton(){
        let btnShowMenu = UIButton(type: UIButton.ButtonType.system)
        btnShowMenu.setImage(self.defaultMenuImage(), for: UIControl.State())
        btnShowMenu.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btnShowMenu.addTarget(self, action: #selector(BaseViewController.onSlideMenuButtonPressed(_:)), for: UIControl.Event.touchUpInside)
        let customBarItem = UIBarButtonItem(customView: btnShowMenu)
        self.navigationItem.leftBarButtonItem = customBarItem;
    }

    func defaultMenuImage() -> UIImage {
        var defaultMenuImage = UIImage()
        
        UIGraphicsBeginImageContextWithOptions(CGSize(width: 30, height: 22), false, 0.0)
        
        UIColor.black.setFill()
        UIBezierPath(rect: CGRect(x: 0, y: 3, width: 30, height: 1)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 10, width: 30, height: 1)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 17, width: 30, height: 1)).fill()
        
        UIColor.white.setFill()
        UIBezierPath(rect: CGRect(x: 0, y: 4, width: 30, height: 1)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 11,  width: 30, height: 1)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 18, width: 30, height: 1)).fill()
        
        defaultMenuImage = UIGraphicsGetImageFromCurrentImageContext()!
        
        UIGraphicsEndImageContext()
       
        return defaultMenuImage;
    }
    
    @objc func onSlideMenuButtonPressed(_ sender : UIButton){
        if (sender.tag == 10)
        {
            // To Hide Menu If it already there
            self.slideMenuItemSelectedAtIndex(-1);
            
            sender.tag = 0;
            
            let viewMenuBack : UIView = view.subviews.last!
            
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                var frameMenu : CGRect = viewMenuBack.frame
                frameMenu.origin.x = -1 * UIScreen.main.bounds.size.width
                viewMenuBack.frame = frameMenu
                viewMenuBack.layoutIfNeeded()
                viewMenuBack.backgroundColor = UIColor.clear
                }, completion: { (finished) -> Void in
                    viewMenuBack.removeFromSuperview()
            })
            
            return
        }
        
        sender.isEnabled = false
        sender.tag = 10
        
        let menuVC : MenuViewController = self.storyboard!.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        menuVC.btnMenu = sender
        menuVC.delegate = self
        self.view.addSubview(menuVC.view)
        self.addChild(menuVC)
        menuVC.view.layoutIfNeeded()
        
        
        menuVC.view.frame=CGRect(x: 0 - UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            menuVC.view.frame=CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
            sender.isEnabled = true
            }, completion:nil)
    }
}
