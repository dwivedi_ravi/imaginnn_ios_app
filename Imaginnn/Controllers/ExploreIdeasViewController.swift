//
//  ExploreIdeasViewController.swift
//  Imaginnn
//
//  Created by Smruti Kanta Mohanty on 02/09/21.
//

import UIKit

class ExploreIdeasViewController: BaseViewController {
    
    @IBOutlet weak var exploreIdeaTableView: UITableView!
    @IBOutlet weak var menuBtn: UIButton!
    
    @IBOutlet weak var filterBtn: UIButton!
    @IBOutlet weak var notificationBtn: UIButton!
    @IBOutlet weak var friendReqBtn: UIButton!
    @IBOutlet weak var searchBtn: UIButton!
    @IBOutlet weak var searchBorderView: BMKCustomView!
    
    var exploreIdeaDataModel = ExploreIdeasDataModel()
    var isPageRefreshing: Bool = false
    var ideasCount: Int = 0
    var offsetCount: Int = 10
    //var IsAPICallAllowed: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       // tempSetValue()
        
        addCustomUIButton(menuBtn)
        setPageUI()
    }
    
    func tempSetValue()  {
        ImaginnnAppManager.shared.selectedColor = ORANGE_COLOR
        ImaginnnAppManager.shared.userId  = "50"
        ImaginnnAppManager.shared.token   = "17b433b3-7180-4a8c-988b-31601ee7cd02"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        exploreIdeaDataModel = ExploreIdeasDataModel()
        exploreIdeaDataModel.ideaItems = [IdeaModel]()
        ideasCount = 0
        getExploreIdeasData()
    }
    

    private func setPageUI() {
        
        exploreIdeaTableView.register(UINib(nibName: "CommonCellView", bundle: nil), forCellReuseIdentifier: "commonCell")
        setSelectedThemeColor()
    }
    
    private func setSelectedThemeColor() {
        
        if let themeColor: UIColor = ImaginnnAppManager.shared.selectedColor {
            searchBorderView.borderColour = themeColor
            self.searchBtn = Helper.changeButtonImageColor("ic_search", themeColor, searchBtn)
            self.filterBtn = Helper.changeButtonImageColor("ic_filter", themeColor, filterBtn)
            self.notificationBtn = Helper.changeButtonImageColor("ic_notification", themeColor, notificationBtn)
            self.friendReqBtn = Helper.changeButtonImageColor("ic_profile_icon_small", themeColor, friendReqBtn)
        }
    }
    
    private func getExploreIdeasData() {
        ImaginnnAppManager.shared.isLoding = true
       // MBProgressHUD.showAdded(to: self.view, animated: true)
        exploreIdeaDataModel.start_count = ideasCount
        exploreIdeaDataModel.postExploreIdeaDataURL(onSuccess: { itemCount in
            ImaginnnAppManager.shared.isLoding = false
            MBProgressHUD.hide(for: self.view, animated: true)
            self.ideasCount += self.offsetCount
            self.isPageRefreshing = false
            self.exploreIdeaTableView.reloadData()
        }) { (error) in
            ImaginnnAppManager.shared.isLoding = false
            MBProgressHUD.hide(for: self.view, animated: true)
            Helper.showToastMessage(message: error?.localizedDescription ?? "", view: self.view)
        }
    }
   
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        let scrooViewPosition = (scrollView.contentSize.height - scrollView.frame.size.height) - scrollView.contentOffset.y
        
        if scrooViewPosition <= 50 {
            if !isPageRefreshing {
                isPageRefreshing = true
                getExploreIdeasData()
            }
        }
    }
    
    @IBAction func filterBtnPressed(_ sender: UIButton) {
        
    }
    
    @IBAction func notificationBtnPressed(_ sender: UIButton) {
        
    }
    
    @IBAction func friendRequestBtnPressed(_ sender: UIButton) {
        
    }
    
    @IBAction func searchBtnPressed(_ sender: UIButton) {
        
    }
}

extension ExploreIdeasViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return exploreIdeaDataModel.ideaItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "commonCell", for: indexPath) as? CommonTableViewCell else {
            return UITableViewCell()
        }
        
        if indexPath.row < exploreIdeaDataModel.ideaItems.count {
            let data = exploreIdeaDataModel.ideaItems[indexPath.row]
            cell.loadExploreIdeaData(data, baseUrl: exploreIdeaDataModel.ideaBaseURL ?? "")
        }
        
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

}
