//
//  CreateIdeaViewController.swift
//  Imaginnn
//
//  Created by Bharat Khatke on 18/09/21.
//

import UIKit

class CreateIdeaViewController: UIViewController {

    @IBOutlet weak var menuBtn: UIButton!
    @IBOutlet weak var addCreatePopUpView: UIView!
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var fieldTextField: UITextField!
    @IBOutlet weak var innerView: BMKCustomView!
    @IBOutlet weak var outerView: BMKCustomView!
    @IBOutlet weak var addImageBtn: UIButton!
    @IBOutlet weak var uploadBtn: UIButton!
    @IBOutlet weak var cameraBtn: UIButton!
    @IBOutlet weak var microMikeBtn: UIButton!
    @IBOutlet weak var editNoteBtn: UIButton!
    @IBOutlet weak var signatureBtn: UIButton!
    
    var imagePicker = UIImagePickerController()
    var capturedImage: UIImage? = nil
    var isAddImageValid: Bool = false
    var selectedCatCode = ""
    var imageUploadModeTag = 0
    
    
    var createDataModel = CreateIdeaDataModel()
    var createIdeaParam:CreateIdeaAPIParam?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addCreatePopUpView.isHidden = true
       // ImaginnnAppManager.shared.selectedColor = ORANGE_COLOR
        UpdateAppData.setSavedUserData()
        setUPUIView()
        getCategoryListFromApi()
    }
    
    
    private func getCategoryListFromApi() {
        
        createDataModel.getCategoryIdeaListDataURL(onSuccess: {
            self.showAnimatedCreateIdeaView()
        }) { (error) in
            print(error?.localizedDescription ?? "")
        }
    }
    
    private func setUPUIView() {
        
       // ImaginnnAppManager.shared.selectedColor = ORANGE_COLOR
        if let themeColor: UIColor = ImaginnnAppManager.shared.selectedColor {
            self.view.backgroundColor = themeColor
            self.outerView.borderColour = themeColor
            self.innerView.borderColour = themeColor
            
            self.menuBtn = Helper.changeButtonImageColor("ic_menu_btn", themeColor, menuBtn)
            self.cameraBtn = Helper.changeButtonImageColor("ic_camera_icon", themeColor, cameraBtn)
            self.uploadBtn = Helper.changeButtonImageColor("ic_upload_icon", themeColor, uploadBtn)
            self.microMikeBtn = Helper.changeButtonImageColor("ic_micro_icon", themeColor, microMikeBtn)
            self.editNoteBtn = Helper.changeButtonImageColor("ic_edit_pen_icon", themeColor, editNoteBtn)
            self.signatureBtn = Helper.changeButtonImageColor("ic_sign_icon", themeColor, signatureBtn)
            
            addImageBtn.layer.cornerRadius = 10
            addImageBtn.layer.masksToBounds = true
        }
        
    }
    
    private func showAnimatedCreateIdeaView() {
        
        UIView.transition(with: self.addCreatePopUpView, duration: 0.5, options: [.curveEaseIn, .transitionCurlUp], animations: {
            self.addCreatePopUpView.isHidden = true
        }) { _ in
            self.view.bringSubviewToFront(self.addCreatePopUpView)
            self.addCreatePopUpView.isHidden = false
        }
    }
    
    private func hideAnimatedCreateIdeaView() {
        
        UIView.transition(with: self.addCreatePopUpView, duration: 1, options: [.curveEaseInOut, .transitionCurlUp], animations: {
            self.addCreatePopUpView.isHidden = false
        }) { _ in
            self.view.bringSubviewToFront(self.addCreatePopUpView)
            self.addCreatePopUpView.isHidden = true
        }
    }
    
    @IBAction func backBtnAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addImageBtnAction(_ sender: UIButton) {
        imageUploadModeTag = 3
        openCamera(true)
    }
    
    @IBAction func goBtnAction(_ sender: UIButton) {
        validateCreateNewIdeaPopUp()
    }
    
    private func validateCreateNewIdeaPopUp() {
        
        let enteredTitle = self.titleTextField.text ?? ""
        let enteredDescription = self.descriptionTextView.text ?? ""
        let enteredCategory = self.selectedCatCode 
        let userID = ImaginnnAppManager.shared.userId ?? ""
        
        self.createIdeaParam =  CreateIdeaAPIParam(enteredTitle, enteredDescription, enteredCategory, userID)
        if enteredTitle.isEmpty {
            Helper.showToastMessage(message: MacroConstant.eneterEmptyFieldMessage("title"), view: self.view)
        } else if enteredDescription.isEmpty {
            Helper.showToastMessage(message: MacroConstant.eneterEmptyFieldMessage("description"), view: self.view)
        } else  if enteredCategory.isEmpty {
            Helper.showToastMessage(message: MacroConstant.eneterEmptyFieldMessage("Please select fields"), view: self.view)
        } else {
            uploadCreateAddIdeaData()
        }
    }
    
    private func uploadCreateAddIdeaData() {
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        createDataModel.createIdeaParam = self.createIdeaParam
        
        createDataModel.postCreateIdeaDataURL(onSuccess: {
            MBProgressHUD.hide(for: self.view, animated: true)
            self.hideAnimatedCreateIdeaView()
        }) { (error) in
            MBProgressHUD.hide(for: self.view, animated: true)
            Helper.showToastMessage(message: error?.localizedDescription ?? "Error", view: self.view)
        }
        
    }
    
    @IBAction func menuBtnAction(_ sender: UIButton) {
        
    }
    @IBAction func uploadDocBtnAction(_ sender: UIButton) {
        imageUploadModeTag = 1
        openCamera(false)
    }
    @IBAction func cameraBtnAction(_ sender: UIButton) {
        imageUploadModeTag = 2
        openCamera(true)
    }
    
    private func openCamera(_ isCamera: Bool = false) {
        
        if isCamera {
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                imagePicker =  UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = .camera
                present(imagePicker, animated: true, completion: nil)
            }
        } else {
            if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
                imagePicker.delegate = self
                imagePicker.sourceType = .savedPhotosAlbum
                imagePicker.allowsEditing = false
                present(imagePicker, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func microBtnAction(_ sender: UIButton) {
        
    }
    @IBAction func editNoteBtnAction(_ sender: UIButton) {
        let viewCtrl = self.storyboard?.instantiateViewController(withIdentifier: "textAlertSegue") as! TextAlertViewController
        viewCtrl.textAlertDelegate = self
        present(viewCtrl, animated: true, completion: nil)
    }
    @IBAction func closeCreateIdeaBtnAction(_ sender: UIButton) {
        self.hideAnimatedCreateIdeaView()
    }
    @IBAction func signatureBtnAction(_ sender: UIButton) {
        let viewCtrl = self.storyboard?.instantiateViewController(withIdentifier: "segnatureSegue") as! SignatureViewController
        viewCtrl.signatureDelegate  = self
        present(viewCtrl, animated: true, completion: nil)
    }
    
    private func showEditIdeaView() {
        
    }
    
    private func callSelectFieldListViewController() {

        let viewCtrl = self.storyboard?.instantiateViewController(withIdentifier: "selectFieldSegue") as! SelectFieldViewController
        
        viewCtrl.selectFieldDelegate = self
        viewCtrl.fieldArray = self.createDataModel.categoryItems
        present(viewCtrl, animated: true, completion: nil)
    }
    
    //MARK:- Capured Images handler
    private func handleCapturedImage(image: UIImage) {
        
        switch imageUploadModeTag {
        case 1:
            //capured Upload image
            break
        case 2:
            //Capured Camera Image
            break
        case 3:
            //add Image
            self.isAddImageValid = true
            self.addImageBtn.setImage(image, for: .normal)
            
        default:
            break
        }
    }
}

extension CreateIdeaViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate, TextAlertDelegate, UITextFieldDelegate, SelectFieldDelegate, SignatureDelegate {
   
    //MARk:- Signature Captured image
    func getSignatureImage(image: UIImage) {
        
    }
    
    //MARK:- Selected Field handler
    func didDoneBtnPressed(data: [CategoryModel]) {
        var selectedCategoryName = ""
        
        for item in data {
            selectedCategoryName += "\(item.categoryName ?? ""),"
            selectedCatCode += "\(item.categoryId ?? 0),"
        }
    
        selectedCatCode = Helper.removeLastCharaterFromString(selectedCatCode)
        self.fieldTextField.text = Helper.removeLastCharaterFromString(selectedCategoryName)
    }
    
    //MARK:- Text Alert message Handler
    func didPressOkButtonAction(_ enteredText: String) {
        print("Create IDea: \(enteredText)")
    }
    
    //MARK:- Image picker delegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
    
    if var image = info[.originalImage] as? UIImage {
        
        if let imageData = image.jpeg(.medium) {
            image = UIImage(data: imageData)!
        }
        self.dismiss(animated: true) {
            self.handleCapturedImage(image: image)
        }
    }
  }
    
    //MARK:- UITextField Delegate methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == fieldTextField {
            return false
        }
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == fieldTextField {
            self.callSelectFieldListViewController()
            return false
        }
        return true
    }
}
