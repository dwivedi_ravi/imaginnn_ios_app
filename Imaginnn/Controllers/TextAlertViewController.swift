//
//  TextAlertViewController.swift
//  Imaginnn
//
//  Created by Smruti Kanta Mohanty on 22/09/21.
//

import UIKit

protocol TextAlertDelegate {
    func didPressOkButtonAction(_ enteredText: String)
}

class TextAlertViewController: UIViewController {

    @IBOutlet weak var enterTextView: UITextView!
    
    var textAlertDelegate: TextAlertDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialViewSetUp()
    }
    
    private func initialViewSetUp() {
        enterTextView.layer.cornerRadius = 8
        enterTextView.layer.borderColor  = UIColor.lightGray.cgColor
        enterTextView.layer.masksToBounds = true
    }
    
    @IBAction func cancelBtnAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func okBtnAction(_ sender: UIButton) {
        
        if !enterTextView.text.isEmpty {
            
            self.dismiss(animated: true) {
                self.textAlertDelegate?.didPressOkButtonAction(self.enterTextView.text ?? "")
            }
        } else {
            Helper.showToastMessage(message: "Field is Empty", view: self.view)
        }
    }
}

/*
extension TextAlertViewController: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
} */
