//
//  LoginViewController.swift
//  Imaginnn
//
//  Created by Smruti Kanta Mohanty on 31/08/21.
//

import UIKit

import FirebaseAuth

class LoginViewController: UIViewController {

    @IBOutlet weak var carousalView: iCarousel!
    @IBOutlet weak var pageControl: BMKPageControl!
    @IBOutlet weak var carousalTitleLbl: UILabel!

    @IBOutlet weak var userNameTF: UITextField!
    @IBOutlet weak var enterPinToLoginLbl: UILabel!
    
    @IBOutlet weak var pin1TF: PinTextField!
    @IBOutlet weak var pin2TF: PinTextField!
    @IBOutlet weak var pin3TF: PinTextField!
    @IBOutlet weak var pin4TF: PinTextField!
    @IBOutlet weak var pin5TF: PinTextField!
    @IBOutlet weak var pin6TF: PinTextField!
    
    @IBOutlet weak var pin1View: BMKCustomView!
    @IBOutlet weak var pin2View: BMKCustomView!
    @IBOutlet weak var pin3View: BMKCustomView!
    @IBOutlet weak var pin4View: BMKCustomView!
    @IBOutlet weak var pin5View: BMKCustomView!
    @IBOutlet weak var pin6View: BMKCustomView!
    
    @IBOutlet weak var innerView: BMKCustomView!
    @IBOutlet weak var outerView: BMKCustomView!
    @IBOutlet weak var showHideBtn: UIButton!
    @IBOutlet weak var forgotPinBtn: UIButton!
    
    @IBOutlet weak var navigationViewHeightConstraint: NSLayoutConstraint!//50
    @IBOutlet weak var carousalViewHeightConstraint: NSLayoutConstraint! //120
    
    //For Forgot Pin
    @IBOutlet weak var enterPinToLoginLblHeightConstraint: NSLayoutConstraint!//21
    @IBOutlet weak var forgotPinViewHeightConstraint: NSLayoutConstraint!//65
    @IBOutlet weak var registerHereBtnViewHeightConstraint: NSLayoutConstraint!//33
    @IBOutlet weak var gotoViewHeightConstraint: NSLayoutConstraint!//90
    @IBOutlet weak var pinStackViewWidthConstraint: NSLayoutConstraint!//320 //210
    @IBOutlet weak var pinStackView: UIStackView!
    
    
    var carouselTimer: Timer!
    var carouselItems = [CarouselData]()
    var isForgotBtnPressed: Bool = false
    var isAlreadyLoggedUser: Bool = false
    var isPinVerification: Bool = false

    private let loginDataModel = LoginDataModel()
    
    private var verification_id: String? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLoginViewUIData()
    }
    
    private func setLoginViewUIData() {
        
        if let data = LoginModel.getCaroselData() {
          carouselItems = data
        }
        
        setSelectedThemeColor()
        setupCarouselView()
        carousalView.reloadData()
        
        if isPinVerification {
            isPinVerification = true
            showSixDigitCodeUI()
        } else if let userName = ImaginnnAppManager.shared.username, !userName.isEmpty {
            isAlreadyLoggedUser = true
            setUpAlreadyLoggedUserUI()
        }
//        if let userID = ImaginnnAppManager.shared.userId, (!userID.isEmpty && isPinVerification) {
//            isPinVerification = true
//            showSixDigitCodeUI()
//        } else if let userName = ImaginnnAppManager.shared.username, !userName.isEmpty {
//            isAlreadyLoggedUser = true
//            setUpAlreadyLoggedUserUI()
//        }
    }
    
    private func setSelectedThemeColor() {
        
        if let themeColor: UIColor = ImaginnnAppManager.shared.selectedColor {
            innerView.borderColour = themeColor
            outerView.borderColour = themeColor
            pin1View.borderColour  = themeColor
            pin2View.borderColour  = themeColor
            pin3View.borderColour  = themeColor
            pin4View.borderColour  = themeColor
            pin5View.borderColour  = themeColor
            pin6View.borderColour  = themeColor

            forgotPinBtn.setTitleColor(themeColor, for: .normal)
            self.view.backgroundColor = themeColor
            
            self.showHideBtn = Helper.changeButtonImageColor("ic_visible_icon", themeColor, showHideBtn)
            
        }
    }
   
    private func setupCarouselView() {
        
        //set the type
        carousalView.type = .linear
        carousalView.alpha = 1
        carousalView.isScrollEnabled = true
        carousalView.isPagingEnabled = true
        
        //set the carousel to move for every 4 sec
        carouselTimer = Timer.scheduledTimer(timeInterval: 4.0, target: self, selector: #selector(moveNext(_:)), userInfo: nil, repeats: true)

        pageControl.numberOfPages = Int(carouselItems.count)
        pageControl.currentPage = 0
        
        // set the delegate and datasource
        carousalView.delegate   = self
        carousalView.dataSource = self
    }
    
    
    @objc func moveNext(_ timer: Timer?) {
        carousalView.scroll(byNumberOfItems: 1, duration: 1.0)
    }
    
    //MARK:- Already Logged user
    private func setUpAlreadyLoggedUserUI() {
        navigationViewHeightConstraint.constant = 0
        carousalViewHeightConstraint.constant = 180
        registerHereBtnViewHeightConstraint.constant = 0
        userNameTF.isHidden = true
        gotoViewHeightConstraint.constant = 0
    }
    
    private func clearPinText() {
        pin1TF.text = ""
        pin2TF.text = ""
        pin3TF.text = ""
        pin4TF.text = ""
        pin5TF.text = ""
        pin6TF.text = ""
    }
    
    private func showSixDigitCodeUI() {
        navigationViewHeightConstraint.constant = 50
        pinStackViewWidthConstraint.constant    = 320
        registerHereBtnViewHeightConstraint.constant = 0
        gotoViewHeightConstraint.constant = 0
        pin5View.isHidden = false
        pin6View.isHidden = false
        userNameTF.isHidden = true
        
        let mobileNo = ImaginnnAppManager.shared.mobile_no ?? ""
        enterPinToLoginLbl.text = "Enter 6 digit code sent to \(mobileNo)"
        forgotPinBtn.setTitle("Resend OTP", for: .normal)
        forgotPinBtn.tag = RESEND_BTN_TAG
        phoneNoAuthentication()
    }
    
    private func phoneNoAuthentication() {
        let mobileNo = ImaginnnAppManager.shared.mobile_no ?? ""
        print(mobileNo)
        if !mobileNo.isEmpty {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            PhoneAuthProvider.provider().verifyPhoneNumber(mobileNo, uiDelegate: nil) { (verificationID, error) in
                MBProgressHUD.hide(for: self.view, animated: true)
                if let err = error {
                    print(err.localizedDescription)
                    Helper.showToastMessage(message: err.localizedDescription , view: self.view)
                    return
                } else {
                    self.verification_id = verificationID
                    UserDefaults.standard.set(verificationID, forKey: "authVerificationID")
                }
            }
        }
    }
    
    private func authenticateOTP(_ enteredOTP: String) {
        verification_id = UserDefaults.standard.string(forKey: "authVerificationID")
        if let verificationID = verification_id {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            let credential = PhoneAuthProvider.provider().credential(withVerificationID: verificationID , verificationCode: enteredOTP)
            Auth.auth().signIn(with: credential) { (authData, error) in
                MBProgressHUD.hide(for: self.view, animated: true)
                if let err = error {
                    Helper.showToastMessage(message: err.localizedDescription, view: self.view)
                } else {
                    //OTP Verified
                    print("Authentication success with:- " + ((authData?.user.phoneNumber) ?? ""))
                   // self.generateNewPinCodePage()
                    self.callCheckOTPAPI()
                }
            }
        } else {
            MBProgressHUD.hide(for: self.view, animated: true)
            Helper.showToastMessage(message: "Error in getting verification ID", view: self.view)
        }
    }
    
    private func callCheckOTPAPI() {
        
        loginDataModel.checkEnteredPINDataURL(onSuccess: {
            self.generateNewPinCodePage()
        }) { (error) in
            Helper.showToastMessage(message: error?.localizedDescription ?? "Error", view: self.view)
        }
    }
    
    private func generateNewPinCodePage() {
        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "generatePinSegue") as? GeneratePinViewController else {
            return
        }
        present(vc, animated: true, completion: nil)
    }
    
    private func loginAlreadyLoggedUserByPinCodeAPI() {
        if let enteredPin = LoginModel.validateAlreadyLoggedUserPin(pin1: pin1TF.text ?? "", pin2: pin2TF.text ?? "", pin3: pin3TF.text ?? "", pin4: pin4TF.text ?? "", pin5: pin5TF.text ?? "", pin6: pin6TF.text ?? "") {
            
          //  authenticateOTP(enteredPin) //temp comments
            callCheckOTPAPI() //temp use
        }
    }
    
    @IBAction func forgotPinBtnAction(_ sender: UIButton) {
        clearPinText()
        if sender.tag == RESEND_BTN_TAG {
            showSixDigitCodeUI()
        } else if isAlreadyLoggedUser {
            showSixDigitCodeUI()
            isForgotBtnPressed = true

        } else {
            loginUserByPinCodeAPI()
            carousalViewHeightConstraint.constant        = 180
            enterPinToLoginLblHeightConstraint.constant  = 0
            forgotPinViewHeightConstraint.constant       = 0
            registerHereBtnViewHeightConstraint.constant = 0
            pinStackView.isHidden = true
            isForgotBtnPressed = true
            forgotPinBtn.tag = FORGOT_BTN_TAG
        }
    }

    @IBAction func viewPinBtnAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected

        if(sender.isSelected == true) {
            pin1TF.isSecureTextEntry = false
            pin2TF.isSecureTextEntry = false
            pin3TF.isSecureTextEntry = false
            pin4TF.isSecureTextEntry = false
            pin5TF.isSecureTextEntry = false
            pin6TF.isSecureTextEntry = false
        } else {
            pin1TF.isSecureTextEntry = true
            pin2TF.isSecureTextEntry = true
            pin3TF.isSecureTextEntry = true
            pin4TF.isSecureTextEntry = true
            pin5TF.isSecureTextEntry = true
            pin6TF.isSecureTextEntry = true
        }
    }
    @IBAction func gotoBtnAction(_ sender: UIButton) {
        if isForgotBtnPressed {
            if let userName = userNameTF.text, !userName.isEmpty {
                checkValidUserName(userName)
            } else {
                Helper.showToastMessage(message: "Please enter username", view: self.view)
            }
        } else {
            checkLoginCredentials()
        }
    }
    
    private func checkLoginCredentials() {
        let userName = userNameTF.text ?? ""
        let enteredPin = LoginModel.validatePin(pin1: pin1TF.text ?? "", pin2: pin2TF.text ?? "", pin3: pin3TF.text ?? "", pin4: pin4TF.text ?? "")
        
        if let loginError = LoginModel.validateUserLogin(userName, enteredPin) {
            Helper.showToastMessage(message: loginError, view: self.view)
        } else {
            loginUserAPI(userName, enteredPin ?? "")
        }
    }
    
    @IBAction func registerHereBtnAction(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let destVC = storyboard.instantiateViewController(withIdentifier: "registerSegue") as! RegistrationViewController
        
        if let appDelegates = appDelegate {
            appDelegates.window?.rootViewController = destVC
        } else {
            present(destVC, animated: true, completion: nil)
        }
    }
    
    //MARK:- Check valid user By API
    private func checkValidUserName(_ userName: String) {
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        loginDataModel.checkValidUserDataURL(parameter: ["username" : userName], onSuccess: {
            MBProgressHUD.hide(for: self.view, animated: true)
            self.handleForgotPinResponse()
        }) { (error) in
            MBProgressHUD.hide(for: self.view, animated: true)
            Helper.showToastMessage(message: error?.localizedDescription ?? "Error", view: self.view)
        }
    }
    
    private func loginUserAPI(_ userName: String, _ pin: String) {
        
        let param = ["username" : userName.removeWhitespace(), "pin" : pin.removeWhitespace()]
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        loginDataModel.loginUserDataURL(parameter: param, onSuccess: {
            MBProgressHUD.hide(for: self.view, animated: true)
            self.callLandingViewController()
        }) { (error) in
            self.clearPinText()
            MBProgressHUD.hide(for: self.view, animated: true)
            Helper.showToastMessage(message: error?.localizedDescription ?? "Error", view: self.view)
        }
    }
    
    //MARK:- Login user by Pin code
    private func loginUserByPinCodeAPI() {
        if let enteredPin = LoginModel.validatePin(pin1: pin1TF.text ?? "", pin2: pin2TF.text ?? "", pin3: pin3TF.text ?? "", pin4: pin4TF.text ?? "") {
            
            if let userName = ImaginnnAppManager.shared.username, !userName.isEmpty {
                loginUserAPI(userName, enteredPin)
            }
        }
    }
    
   
    
    private func callLandingViewController() {
        guard let destVC = storyboard?.instantiateViewController(withIdentifier: SEGUES.landingPageSegue.rawValue) else {
            return
        }
        
        if let appDelegates = appDelegate {
            appDelegates.window?.rootViewController = destVC
        } else {
            present(destVC, animated: true, completion: nil)
        }
    }
    
    private func handleForgotPinResponse() {
        if !(self.loginDataModel.validUserData?.result ?? false) {
            if let message = self.loginDataModel.validUserData?.remark {
                Helper.showToastMessage(message: message, view: self.view)
            }
        }
    }
    
    //MARK:- For Pin TextField
    func textFieldDelete(textField: PinTextField) {
        switch textField{
        case pin2TF:
            pin1TF.becomeFirstResponder()
            
        case pin3TF:
            pin2TF.becomeFirstResponder()
            
        case pin4TF:
            pin3TF.becomeFirstResponder()
            
        case pin5TF:
            pin4TF.becomeFirstResponder()
            
        case pin6TF:
            pin5TF.becomeFirstResponder()
            
        case pin1TF:
            pin1TF.resignFirstResponder()
        default:
            break
        }
    }
}

extension LoginViewController: iCarouselDelegate, iCarouselDataSource, UITextFieldDelegate, PinTexFieldDelegate {
    
    func numberOfItems(in carousel: iCarousel) -> Int {
        return carouselItems.count
    }
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        var carouselView = view
        
         carouselView = UIView(frame: CGRect(x: 0, y: 0, width: carousalView.frame.width, height: carousalView.frame.height))
        
        return carouselView!
    }
    
    func carouselCurrentItemIndexDidChange(_ carousel: iCarousel) {
        let index: Int = carousel.currentItemIndex
        pageControl.currentPage = index
        carousalTitleLbl.text = carouselItems[index].titleName
    }
    
    func carousel(_ carousel: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        switch option {
        case iCarouselOption.wrap:
            return 1
        default:
            return value
        }
    }
    
    func didPressBackspace(textField: PinTextField) {
        textFieldDelete(textField: textField)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == pin1TF || textField == pin2TF || textField == pin3TF || textField == pin4TF || textField == pin5TF || textField == pin6TF {
            if (textField.text?.count ?? 0 < 1) && (string.count > 0) {
                
                switch textField{
                case pin1TF:
                    pin2TF.becomeFirstResponder()
                    
                case pin2TF:
                    pin3TF.becomeFirstResponder()
                    
                case pin3TF:
                    pin4TF.becomeFirstResponder()
                    
                case pin4TF:
                    if (isAlreadyLoggedUser && isForgotBtnPressed) || isPinVerification {
                        pin5TF.becomeFirstResponder()
                    } else {
                        pin4TF.text = string
                        pin4TF.resignFirstResponder()
                        loginUserByPinCodeAPI()
                    }
    
                case pin5TF:
                    pin6TF.becomeFirstResponder()
                    
                case pin6TF:
                    pin6TF.text = string
                    pin6TF.resignFirstResponder()
                    loginAlreadyLoggedUserByPinCodeAPI()
                    
                default:
                    break
                }
                
                textField.text = string
                return false
            }else if textField.text?.count ?? 0 >= 1 && string.count == 0 {
              
                textFieldDelete(textField: textField as! PinTextField)
                
                textField.text = ""
                textField.textColor = .darkGray
                return false
            } else if textField.text?.count ?? 0 >= 1 {
                textField.text = string
                return false
            }
        }
        
        return true
    }
}
