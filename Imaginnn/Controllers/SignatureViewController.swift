//
//  SignatureViewController.swift
//  Imaginnn
//
//  Created by Bharat Khatke on 22/09/21.
//

import UIKit

protocol SignatureDelegate {
    func getSignatureImage(image: UIImage)
}

class SignatureViewController: UIViewController {

    @IBOutlet weak var signatureView: YPDrawSignatureView!
    @IBOutlet weak var signatureSlider: UISlider!
    @IBOutlet weak var strockView: BMKCustomView!
    @IBOutlet weak var strockViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var colorViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var sliderViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var colorCollectionView: UICollectionView!
    
    var isMultiple: Float = 1.0
    var maxSliderValue: Float = 10.0
    var minSliderValue: Float = 1.0
    var isFontSlider: Bool = false
    var selectedColor: UIColor = .black
    
    var capturedImage: UIImage? = nil
    var signatureColorList = SignatureColor.getSignatureColorSet()
    
    var signatureDelegate: SignatureDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideAllSubView()
    }
    
    @IBAction func backBtnAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveBtnAction(_ sender: UIButton) {
        if let signatureImage = self.signatureView.getSignature(scale: 1) {
            if let imageData = signatureImage.jpeg(.medium) {
                self.capturedImage = UIImage(data: imageData)!
            }
            if let image = self.capturedImage {
               UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
                dismiss(animated: true) {
                    self.signatureDelegate?.getSignatureImage(image: image)
                }
            }
        }
    }
    
    @IBAction func clearBtnAction(_ sender: UIButton) {
        self.signatureView.clear()
        hideAllSubView()
    }
    
    @IBAction func colorBtnAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        
        if sender.isSelected == true {
            hideAllSubView()
            colorViewHeightConstraint.constant = 50
            bottomViewHeightConstraint.constant = 100
            colorCollectionView.reloadData()
        } else {
            hideAllSubView()
        }
    }
    
    private func hideAllSubView() {
        bottomViewHeightConstraint.constant = 50
        colorViewHeightConstraint.constant  = 0
        sliderViewHeightConstraint.constant = 0
    }
    
    @IBAction func opacityBtnAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        
        if sender.isSelected == true {
            self.setSliderValueOf()
            hideAllSubView()
            sliderViewHeightConstraint.constant = 50
            bottomViewHeightConstraint.constant = 100
            
        } else {
            hideAllSubView()
        }
    }
    
    @IBAction func fontWidthBtnAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        
        if sender.isSelected == true {
            self.setSliderValueOf(isFont: true)
            hideAllSubView()
            sliderViewHeightConstraint.constant = 50
            bottomViewHeightConstraint.constant = 100

        } else {
            hideAllSubView()
        }
        
    }
    
    private func setSliderValueOf(isFont: Bool = false) {
        if isFont {
            signatureSlider.minimumValue = 1
            signatureSlider.maximumValue = 15
            signatureSlider.value        = 2.0
            isMultiple = 1.0
            isFontSlider = true
            
            
            
        } else {
            signatureSlider.minimumValue = 0.1
            signatureSlider.maximumValue = 1.0
            signatureSlider.value        = 1.0
            isMultiple = 0.1
            isFontSlider = false
            strockView.backgroundColor = selectedColor
        }
    }
    
    @IBAction func signatureSliderValueChanged(_ sender: UISlider) {
        var sliderValue: Float = 0.0
        sliderValue = roundf(sender.value / isMultiple) * isMultiple
        
        if isFontSlider {
            let value = Int(sliderValue)
            signatureView.strokeWidth = CGFloat(value)
            
            strockViewWidthConstraint.constant = CGFloat((value * 2))
            strockView.cornerRadius = CGFloat(value)
            
        } else {
            
            signatureView.strokeColor  = selectedColor.withAlphaComponent(CGFloat(sliderValue))
            strockView.backgroundColor = selectedColor.withAlphaComponent(CGFloat(sliderValue))
        }
    }
}

extension SignatureViewController: YPSignatureDelegate {
    func didStart(_ view : YPDrawSignatureView) { }
    
    func didFinish(_ view : YPDrawSignatureView) {}
}

extension SignatureViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return signatureColorList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "colorCollectionCell", for: indexPath) as? ColorCollectionCell  else {
            return UICollectionViewCell()
        }
        
        let cellData = signatureColorList[indexPath.item]
        cell.setColorCellData(item: cellData)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        signatureColorList.indices.forEach { signatureColorList[$0].index = 0 }
       
        let item = signatureColorList[indexPath.item]
        signatureView.strokeColor = item.color
        selectedColor = item.color
        signatureColorList[indexPath.item].index = 1
        collectionView.reloadData()
    }
    
}
