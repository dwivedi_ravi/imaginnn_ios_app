//
//  InitialViewController.swift
//  Imaginnn
//
//  Created by Smruti Kanta Mohanty on 31/08/21.
//

import UIKit


class InitialViewController: UIViewController {

    @IBOutlet weak var setColorCollectionView: UICollectionView!
    
    let numberOfmoduleCellsPerRow: CGFloat = 4
    private var colorListVM: InitialViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        colorListVM = InitialViewModel()
        setColorCollectionView.reloadData()
    }
    
    private func checkAndUpdateAlreadyLoginUserDetails() {
        
        if let userID = AppUtility.getUserDetailsBy(key: .userId), !userID.isEmpty {
            UpdateAppData.setSavedUserData()
            performSegue(withIdentifier: "toLoginPageSegue", sender: self)
        } else {
            performSegue(withIdentifier: "registratinSegue", sender: self)
        }
    }
    
    override func viewDidLayoutSubviews() {
        
        if let flowLayout = setColorCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            let horizontalSpacing = flowLayout.scrollDirection == .vertical ? flowLayout.minimumInteritemSpacing : flowLayout.minimumLineSpacing
            let cellWidth = (self.setColorCollectionView.frame.width - max(0, numberOfmoduleCellsPerRow - 1)*(horizontalSpacing))/numberOfmoduleCellsPerRow
            flowLayout.itemSize = CGSize(width: cellWidth-10, height: 50)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "registratinSegue" {
            let vc = segue.destination as! RegistrationViewController
            present(vc, animated: true, completion: nil)
        }
    }
}

extension InitialViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return  (self.colorListVM != nil) ? self.colorListVM.numberOfRowsInSection() : 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "colorSetCell", for: indexPath) as? SetColorCell else {
            return UICollectionViewCell()
        }
        
        let cellData = self.colorListVM.cellItemAtIndex(indexPath.row)
        cell.innerView.backgroundColor = cellData.color
        cell.outerView.borderColour    = cellData.color
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cellData = self.colorListVM.cellItemAtIndex(indexPath.row)
        ImaginnnAppManager.shared.selectedColor = cellData.color
        checkAndUpdateAlreadyLoginUserDetails()
    }
}
