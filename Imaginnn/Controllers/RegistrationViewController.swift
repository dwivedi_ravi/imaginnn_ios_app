//
//  RegistrationViewController.swift
//  Imaginnn
//
//  Created by Smruti Kanta Mohanty on 31/08/21.
//

import UIKit
import iOSDropDown

class RegistrationViewController: UIViewController {

    @IBOutlet weak var usernameTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var mobileNoTF: UITextField!
    @IBOutlet weak var countryNameTF: UITextField!
   // @IBOutlet weak var countryDropDown: DropDown!
    @IBOutlet weak var countryCodeView: UIView!
    @IBOutlet weak var innerView: BMKCustomView!
    @IBOutlet weak var outerView: BMKCustomView!
    @IBOutlet weak var gotItInnerView: BMKCustomView!
    @IBOutlet weak var gotItOuterView: BMKCustomView!
    @IBOutlet weak var registrationFlashView: UIView!
    
    private let registrationDataModel = RegistrationDataModel()
    var selectedCountry: String = "India"
    private let loginDataModel = LoginDataModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        setRegistrationUI()
    }
    
    private func setRegistrationUI() {
        mobileNoTF.leftViewMode = .always
        mobileNoTF.leftView     = countryCodeView
        countryNameTF.text      = "India"
        if let themeColor: UIColor = ImaginnnAppManager.shared.selectedColor {
            innerView.borderColour = themeColor
            outerView.borderColour = themeColor
            gotItOuterView.borderColour = themeColor
            gotItInnerView.borderColour = themeColor
        }
        usernameTF.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
     //   getCountryListData()
    }
    
    
   
    
    //MARK:- TextField Delegate
    @objc func textFieldDidChange(textField: UITextField) {
        let newString = textField.text!
        let filterText = newString.filterExtraWhitespaces()
        if textField == usernameTF {
            if filterText?.count ?? 0 > 2 {
                checkValidUserName(filterText ?? "")
            } else {
                
            }
        }
        
    }
    
    @IBAction func gotoBtnAction(_ sender: UIButton) {
        registerUserAPI()
    }
    
    @IBAction func gotItBtnAction(_ sender: UIButton) {
        registrationFlashView.isHidden = true
    }
    
    //MARK:- User Registartion API
    private func registerUserAPI() {
        
        registrationDataModel.userName = usernameTF.text ?? ""
        registrationDataModel.emailID  = emailTF.text ?? ""
        registrationDataModel.mobileNo = mobileNoTF.text ?? ""
        registrationDataModel.country  = countryNameTF.text ?? ""
        
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        registrationDataModel.registerUserDataURL(onSuccess: {
            MBProgressHUD.hide(for: self.view, animated: true)
            self.callLoginPage(true)
        }) { (error) in
            MBProgressHUD.hide(for: self.view, animated: true)
            Helper.showToastMessage(message: error?.localizedDescription ?? "", view: self.view)
        }
    }
    //7738815078
    private func callLoginPage(_ isAfterRegistration: Bool = false) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let destVC = storyboard.instantiateViewController(withIdentifier: "loginSegue") as! LoginViewController
        destVC.isPinVerification = isAfterRegistration
        if let appDelegates = appDelegate {
            appDelegates.window?.rootViewController = destVC
        } else {
            present(destVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func alreadyRegisterBtnAction(_ sender: UIButton) {
        callLoginPage()
    }
    
    //MARK:- Check valid user By API
    private func checkValidUserName(_ userName: String) {
        
        loginDataModel.checkValidUserDataURL(parameter: ["username" : userName], onSuccess: {
            self.handleUserNameTextField()
        }) { (error) in
            Helper.showToastMessage(message: error?.localizedDescription ?? "Error", view: self.view)
        }
    }
    
    private func handleUserNameTextField() {
        let message = loginDataModel.validUserData?.remark ?? "User name"
        if loginDataModel.validUserData?.result ?? false {
            usernameTF.placeholder = message
        } else {
            usernameTF.placeholder = "User name"
        }
    }
    
    //MARK:- not using now
    /*
    private func getCountryListData()  {
        //MBProgressHUD.showAdded(to: self.view, animated: true)
        registrationDataModel.getCountryDataURL(onSuccess: {
            MBProgressHUD.hide(for: self.view, animated: true)
            self.setCountryListData()
        }) { (error) in
            MBProgressHUD.hide(for: self.view, animated: true)
            Helper.showToastMessage(message: error?.localizedDescription ?? "Error", view: self.view)
        }
    }
    
    private func setCountryListData() {
        let  contryList = ["India"]
        countryDropDown.optionArray = contryList
       // countryDropDown.optionArray = registrationDataModel.countryList
        countryDropDown.didSelect { (selectedText, index, id) in
            self.selectedCountry = selectedText
        }
        if !countryDropDown.optionArray.isEmpty {
            selectedCountry = countryDropDown.optionArray[0]
            self.countryDropDown.text = selectedCountry
        }
    } */

}

extension RegistrationViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let currentString: NSString = textField.text as NSString? else {
            return false
        }
        switch textField {
        case mobileNoTF:
            
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= MOBILE_NO_MAX_LENGTH
            
        case emailTF:

            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= EMAIL_ID_MAX_LENGTH
            
        default:
            break
        }
        
        return true
    }
}
