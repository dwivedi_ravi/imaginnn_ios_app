//
//  FavoriteIdeasViewController.swift
//  Imaginnn
//
//  Created by Smruti Kanta Mohanty on 02/09/21.
//

import UIKit

class FavoriteIdeasViewController: BaseViewController {

    @IBOutlet weak var menuBtn: UIButton!
    @IBOutlet weak var searchBtn: UIButton!
    @IBOutlet weak var searchBorderView: BMKCustomView!
    @IBOutlet weak var favoriteIdeaTableView: UITableView!
    
    var isPageRefreshing: Bool = false
    var ideasCount: Int = 0
    var offsetCount: Int = 10
    var IsAPICallAllowed: Bool = true
    var favoriteIdeaDataModel = FavoriteIdeasDataModel()
    
    override func viewDidLoad() {
        addCustomUIButton(menuBtn)
        super.viewDidLoad()
        setPageUI()
       // tempSetValue()
        setSelectedThemeColor()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        favoriteIdeaDataModel = FavoriteIdeasDataModel()
        favoriteIdeaDataModel.favoriteIdeaItems = [IdeaModel]()
        ideasCount = 0
        getExploreIdeasData()
    }
    
    func tempSetValue()  {
        ImaginnnAppManager.shared.selectedColor = ORANGE_COLOR
        ImaginnnAppManager.shared.userId  = "50"
        ImaginnnAppManager.shared.token   = "17b433b3-7180-4a8c-988b-31601ee7cd02"
    }
    
    private func setPageUI() {
        favoriteIdeaTableView.register(UINib(nibName: "CommonCellView", bundle: nil), forCellReuseIdentifier: "commonCell")
        setSelectedThemeColor()
        favoriteIdeaTableView.reloadData()
    }
    
    private func setSelectedThemeColor() {
        
        if let themeColor: UIColor = ImaginnnAppManager.shared.selectedColor {
            searchBorderView.borderColour = themeColor
            self.searchBtn = Helper.changeButtonImageColor("ic_search", themeColor, searchBtn)
        }
    }
    
    private func getExploreIdeasData() {
      //  MBProgressHUD.showAdded(to: self.view, animated: true)
        ImaginnnAppManager.shared.isLoding = true
        if (favoriteIdeaDataModel.start_count != ideasCount) || ideasCount == 0 {
            favoriteIdeaDataModel.start_count = ideasCount
            favoriteIdeaDataModel.postFavoriteIdeaDataURL(onSuccess: { itemCount in
                _ = (itemCount == self.offsetCount) ? (self.IsAPICallAllowed = true) : (self.IsAPICallAllowed = false)
                ImaginnnAppManager.shared.isLoding = false
                MBProgressHUD.hide(for: self.view, animated: true)
                self.ideasCount += self.offsetCount
                self.isPageRefreshing = false
                self.favoriteIdeaTableView.reloadData()
            }) { (error) in
                self.IsAPICallAllowed = true
                ImaginnnAppManager.shared.isLoding = false
                MBProgressHUD.hide(for: self.view, animated: true)
                Helper.showToastMessage(message: error?.localizedDescription ?? "", view: self.view)
            }
        }
     }
    
     
     func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
         let scrooViewPosition = (scrollView.contentSize.height - scrollView.frame.size.height) - scrollView.contentOffset.y
         
         if scrooViewPosition <= 50 {
             if !isPageRefreshing {
                 isPageRefreshing = true
                if IsAPICallAllowed {
                    getExploreIdeasData()
                }
                 
             }
         }
     }
    
    @IBAction func searchBtnPressed(_ sender: UIButton) {
        
    }
    
}

extension FavoriteIdeasViewController: UITableViewDataSource, UITableViewDelegate  {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return favoriteIdeaDataModel.favoriteIdeaItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "commonCell", for: indexPath) as? CommonTableViewCell else {
            return UITableViewCell()
        }
        
        let cellData = favoriteIdeaDataModel.favoriteIdeaItems[indexPath.row]
         cell.loadExploreIdeaData(cellData, baseUrl: favoriteIdeaDataModel.ideaBaseURL ?? "")
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
