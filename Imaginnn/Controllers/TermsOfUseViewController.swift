//
//  TermsOfUseViewController.swift
//  Imaginnn
//
//  Created by Bharat Khatke on 06/09/21.
//

import UIKit

class TermsOfUseViewController: UIViewController {

    @IBOutlet weak var exploreBtn: UIButton!
    @IBOutlet weak var myvaultBtn: UIButton!
    @IBOutlet weak var personBtn: UIButton!
    @IBOutlet weak var favoriteBtn: UIButton!
    @IBOutlet weak var filterBtn: UIButton!
    @IBOutlet weak var userBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
     //   setSelectedThemeColor()

    }
    
    private func setSelectedThemeColor() {
        
       // ImaginnnAppManager.shared.selectedColor = GREEN_COLOR
        if let themeColor: UIColor = ImaginnnAppManager.shared.selectedColor {
  
            self.exploreBtn = Helper.changeButtonImageColor("ic_explore_idea_tab", themeColor, exploreBtn)
            self.myvaultBtn = Helper.changeButtonImageColor("ic_myVault_tab", themeColor, myvaultBtn)
            self.personBtn = Helper.changeButtonImageColor("ic_people_tab", themeColor, personBtn)
            self.favoriteBtn = Helper.changeButtonImageColor("ic_favorite_tab", themeColor, favoriteBtn)
            self.filterBtn = Helper.changeButtonImageColor("ic_filter", themeColor, filterBtn)
            self.userBtn = Helper.changeButtonImageColor("ic_profile_icon", themeColor, userBtn)
        }
    }
    
    @IBAction func backBtnAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
