//
//  GeneratePinViewController.swift
//  Imaginnn
//
//  Created by Bharat Khatke on 05/09/21.
//

import UIKit

class GeneratePinViewController: UIViewController {
    
    @IBOutlet weak var carousalView: iCarousel!
    @IBOutlet weak var pageControl: BMKPageControl!
    @IBOutlet weak var carousalTitleLbl: UILabel!
    
    @IBOutlet weak var pinCode1TF: PinTextField!
    @IBOutlet weak var pinCode2TF: PinTextField!
    @IBOutlet weak var pinCode3TF: PinTextField!
    @IBOutlet weak var pinCode4TF: PinTextField!
    
    @IBOutlet weak var pin1View: BMKCustomView!
    @IBOutlet weak var pin2View: BMKCustomView!
    @IBOutlet weak var pin3View: BMKCustomView!
    @IBOutlet weak var pin4View: BMKCustomView!
    
    
    
    @IBOutlet weak var confirmPin1TF: PinTextField!
    @IBOutlet weak var confirmPin2TF: PinTextField!
    @IBOutlet weak var confirmPin3TF: PinTextField!
    @IBOutlet weak var confirmPin4TF: PinTextField!
    
    @IBOutlet weak var confirmPin1View: BMKCustomView!
    @IBOutlet weak var confirmPin2View: BMKCustomView!
    @IBOutlet weak var confirmPin3View: BMKCustomView!
    @IBOutlet weak var confirmPin4View: BMKCustomView!
    
    @IBOutlet weak var innerView: BMKCustomView!
    @IBOutlet weak var outerView: BMKCustomView!
    @IBOutlet weak var showHideEnterBtn: UIButton!
    @IBOutlet weak var showHideReEnterBtn: UIButton!
    
    
    var carouselItems = [CarouselData]()
    var carouselTimer: Timer!
    var loginDataModel = LoginDataModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        setGeneratePinUI()
    }
    
    
    private func setGeneratePinUI() {
        
        if let data = LoginModel.getCaroselData() {
          carouselItems = data
        }
        setSelectedThemeColor()
        setupCarouselView()
        carousalView.reloadData()
    }
    
    
    private func setSelectedThemeColor() {
        
        if let themeColor: UIColor = ImaginnnAppManager.shared.selectedColor {
            innerView.borderColour = themeColor
            outerView.borderColour = themeColor
            pin1View.borderColour  = themeColor
            pin2View.borderColour  = themeColor
            pin3View.borderColour  = themeColor
            pin4View.borderColour  = themeColor
            
            confirmPin1View.borderColour  = themeColor
            confirmPin2View.borderColour  = themeColor
            confirmPin3View.borderColour  = themeColor
            confirmPin4View.borderColour  = themeColor
        
            self.view.backgroundColor = themeColor
            
            self.showHideEnterBtn = Helper.changeButtonImageColor("ic_visible_icon", themeColor, showHideEnterBtn)
            self.showHideReEnterBtn = Helper.changeButtonImageColor("ic_visible_icon", themeColor, showHideReEnterBtn)
            
            Helper.showToastMessage(message: MacroConstant.SHOW_PIN_GENERATION_MSG, view: self.view)
            
        }
    }
    
    private func clearGeneratePinText() {
        pinCode1TF.text = ""
        pinCode2TF.text = ""
        pinCode3TF.text = ""
        pinCode4TF.text = ""
        confirmPin1TF.text = ""
        confirmPin2TF.text = ""
        confirmPin3TF.text = ""
        confirmPin4TF.text = ""
    }
    
    private func setupCarouselView() {
        
        //set the type
        carousalView.type = .linear
        carousalView.alpha = 1
        carousalView.isScrollEnabled = true
        carousalView.isPagingEnabled = true
        
        //set the carousel to move for every 4 sec
        carouselTimer = Timer.scheduledTimer(timeInterval: 4.0, target: self, selector: #selector(moveNext(_:)), userInfo: nil, repeats: true)

        pageControl.numberOfPages = Int(carouselItems.count)
        pageControl.currentPage = 0
        
        // set the delegate and datasource
        carousalView.delegate   = self
        carousalView.dataSource = self
    }
    
    
    @objc func moveNext(_ timer: Timer?) {
        carousalView.scroll(byNumberOfItems: 1, duration: 1.0)
    }
    
    //MARK:- Login user by Pin code
    private func validateEnteredPinCode()  {
        
        guard let enteredPin = GeneratePinModel.validatePin(pin1: pinCode1TF.text ?? "", pin2: pinCode2TF.text ?? "", pin3: pinCode3TF.text ?? "", pin4: pinCode4TF.text ?? "") else {
            Helper.showToastMessage(message: MacroConstant.PLEASE_ENTER_VALID_PIN, view: self.view)
            return
        }
        
        guard let reEnteredPin = GeneratePinModel.validatePin(pin1: confirmPin1TF.text ?? "", pin2: confirmPin2TF.text ?? "", pin3: confirmPin3TF.text ?? "", pin4: confirmPin4TF.text ?? "") else {
            Helper.showToastMessage(message: MacroConstant.PLEASE_ENTER_VALID_PIN, view: self.view)
            return
        }
        
        if let validateBothPin = GeneratePinModel.isValidEnteredPinCode(enteredPin: enteredPin, reEnteredPin: reEnteredPin) {
            Helper.showToastMessage(message: validateBothPin, view: self.view)
        } else {
            print("Pin code matching")
            callPinCodeGenerateAPI(enteredPin)
        }
    }
    
    private func callPinCodeGenerateAPI(_ pin: String) {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        loginDataModel.updateEneteredPINDataURL(pin, onSuccess: {
            MBProgressHUD.hide(for: self.view, animated: true)
            
            self.callNextPage(pin)
        }) { (error) in
            MBProgressHUD.hide(for: self.view, animated: true)
            Helper.showToastMessage(message: error?.localizedDescription ?? "Error", view: self.view)
        }
    }
    
    private func callNextPage(_ pin: String) {
        
        AppUtility.setValueWithKeyInKeyChain(value: pin, forkey: UserDetails.pin.rawValue)
        ImaginnnAppManager.shared.pin = pin
        
        guard let destVC = storyboard?.instantiateViewController(withIdentifier: "landingPageSegue")  else {
            return
        }
        
        if let appDelegates = appDelegate {
            appDelegates.window?.rootViewController = destVC
        } else {
            present(destVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func showHidePinTextBtnAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected

        if sender.tag == 101 {
            if(sender.isSelected == true) {
                pinCode1TF.isSecureTextEntry = false
                pinCode2TF.isSecureTextEntry = false
                pinCode3TF.isSecureTextEntry = false
                pinCode4TF.isSecureTextEntry = false

            } else {
                pinCode1TF.isSecureTextEntry = true
                pinCode2TF.isSecureTextEntry = true
                pinCode3TF.isSecureTextEntry = true
                pinCode4TF.isSecureTextEntry = true
            }
        } else {
            if(sender.isSelected == true) {
                confirmPin1TF.isSecureTextEntry = false
                confirmPin2TF.isSecureTextEntry = false
                confirmPin3TF.isSecureTextEntry = false
                confirmPin4TF.isSecureTextEntry = false

            } else {
                confirmPin1TF.isSecureTextEntry = true
                confirmPin2TF.isSecureTextEntry = true
                confirmPin3TF.isSecureTextEntry = true
                confirmPin4TF.isSecureTextEntry = true
            }
        }
    }
    
    @IBAction func gotoBtnAction(_ sender: UIButton) {
        validateEnteredPinCode()
    }
    
    func textFieldDelete(textField: PinTextField) {
        
        switch textField{
        case pinCode2TF:
            pinCode1TF.becomeFirstResponder()
            
        case pinCode3TF:
            pinCode2TF.becomeFirstResponder()
            
        case pinCode4TF:
            pinCode3TF.becomeFirstResponder()
            
        case pinCode1TF:
            pinCode1TF.resignFirstResponder()
            
        case confirmPin2TF:
            confirmPin1TF.becomeFirstResponder()
               
        case confirmPin3TF:
            confirmPin2TF.becomeFirstResponder()
               
        case confirmPin4TF:
            confirmPin3TF.becomeFirstResponder()
               
        case confirmPin1TF:
            confirmPin1TF.resignFirstResponder()
            
        default:
            break
        }
    }
}

extension GeneratePinViewController: iCarouselDelegate, iCarouselDataSource, UITextFieldDelegate, PinTexFieldDelegate {
   
    func numberOfItems(in carousel: iCarousel) -> Int {
        return carouselItems.count
    }
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        var carouselView = view
        
         carouselView = UIView(frame: CGRect(x: 0, y: 0, width: carousalView.frame.width, height: carousalView.frame.height))
        
        return carouselView!
    }
    
    func carouselCurrentItemIndexDidChange(_ carousel: iCarousel) {
        let index: Int = carousel.currentItemIndex
        pageControl.currentPage = index
        carousalTitleLbl.text = carouselItems[index].titleName
    }
    
    func carousel(_ carousel: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        switch option {
        case iCarouselOption.wrap:
            return 1
        default:
            return value
        }
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if (textField.text?.count ?? 0 < 1) && (string.count > 0) {
            
           switch textField.tag {
           case 1, 2, 3, 4:
               switch textField{
               case pinCode1TF:
                    pinCode2TF.becomeFirstResponder()
               case pinCode2TF:
                    pinCode3TF.becomeFirstResponder()
               case pinCode3TF:
                    pinCode4TF.becomeFirstResponder()
               case pinCode4TF:
                    pinCode4TF.text = string
                    pinCode4TF.resignFirstResponder()
               default:
                   break
               }
               
           case 5, 6, 7, 8:
               switch textField{
               case confirmPin1TF:
                   confirmPin2TF.becomeFirstResponder()
                   
               case confirmPin2TF:
                   confirmPin3TF.becomeFirstResponder()
                   
               case confirmPin3TF:
                   confirmPin4TF.becomeFirstResponder()
                   
               case confirmPin4TF:
                   confirmPin4TF.resignFirstResponder()
                   confirmPin4TF.text = string
                
               default:
                   break
               }
           
           default:
               break
           }
            
            textField.text = string
            return false
        }else if textField.text?.count ?? 0 >= 1 && string.count == 0 {
            
            textFieldDelete(textField: textField as! PinTextField)
           
            textField.text = ""
            return false
        } else if textField.text?.count ?? 0 >= 1 {
            textField.text = string
            return false
        }
        
        
        return true
    }
    
    func didPressBackspace(textField: PinTextField) {
        textFieldDelete(textField: textField)
    }
}

