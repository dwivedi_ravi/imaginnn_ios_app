//
//  MenuViewController.swift
//  Imaginnn
//
//  Created by Smruti Kanta Mohanty on 03/09/21.
//

import UIKit

struct MenuModel {
    var title: String
    var icon: String
}

enum MENULIST: String {
    
    case changePin     = "Change Pin"
    case termsOfUse    = "Terms of Use"
    case rateTheApp    = "Rate the App"
    case contactUs     = "Contact Us"
    case inviteFriends = "Invite Friends"
    case faqs          = "FAQ's"
    case logout        = "Logout"
}



protocol SlideMenuDelegate {
    func slideMenuItemSelectedAtIndex(_ index : Int32)
}

class MenuViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var usernameLbl: UILabel! //user name
    @IBOutlet weak var profilePicBtn: UIButton!
    
    /**
    *  Array to display menu options
    */
    @IBOutlet var tblMenuOptions : UITableView!
    
    /**
    *  Transparent button to hide menu
    */
    @IBOutlet var btnCloseMenuOverlay : UIButton!
    
    /**
    *  Array containing menu options
    */
    var arrayMenuOptions = [MenuModel]()
    
    /**
    *  Menu button which was tapped to display the menu
    */
    var btnMenu : UIButton!
    
    /**
    *  Delegate of the MenuVC
    */
    var delegate : SlideMenuDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let userName = ImaginnnAppManager.shared.username {
            usernameLbl.text = userName
        }
        
        tblMenuOptions.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateArrayMenuOptions()
    }
    
    private func updateArrayMenuOptions(){
       
        arrayMenuOptions.append(MenuModel(title: MENULIST.changePin.rawValue, icon: ""))
        arrayMenuOptions.append(MenuModel(title: MENULIST.termsOfUse.rawValue, icon: ""))
        arrayMenuOptions.append(MenuModel(title: MENULIST.rateTheApp.rawValue, icon: ""))
        arrayMenuOptions.append(MenuModel(title: MENULIST.contactUs.rawValue, icon: ""))
        arrayMenuOptions.append(MenuModel(title: MENULIST.inviteFriends.rawValue, icon: ""))
        arrayMenuOptions.append(MenuModel(title: MENULIST.faqs.rawValue, icon: ""))
        arrayMenuOptions.append(MenuModel(title: MENULIST.logout.rawValue, icon: ""))
        
        tblMenuOptions.reloadData()
    }
    
    @IBAction func myProfileBtnAction(_ sender: UIButton) {
        sender.tag = 101
        onCloseMenuClick(sender)
      //  delegate?.slideMenuItemSelectedAtIndex(101)
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.view.frame = CGRect(x: -UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width,height: UIScreen.main.bounds.size.height)
            self.view.layoutIfNeeded()
            self.view.backgroundColor = UIColor.clear
            }, completion: { (finished) -> Void in
                self.view.removeFromSuperview()
                self.removeFromParent()
        })
    }
    
    @IBAction func onCloseMenuClick(_ button:UIButton!){
        btnMenu.tag = 0
        
        if (self.delegate != nil) {
            var index = Int32(button.tag)
            if(button == self.btnCloseMenuOverlay){
                index = -1
            }
            delegate?.slideMenuItemSelectedAtIndex(index)
        }
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.view.frame = CGRect(x: -UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width,height: UIScreen.main.bounds.size.height)
            self.view.layoutIfNeeded()
            self.view.backgroundColor = UIColor.clear
            }, completion: { (finished) -> Void in
                self.view.removeFromSuperview()
                self.removeFromParent()
        })
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cellMenu")!
        
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        cell.layoutMargins = UIEdgeInsets.zero
        cell.preservesSuperviewLayoutMargins = false
        cell.backgroundColor = UIColor.clear
        
        let lblTitle : UILabel = cell.contentView.viewWithTag(101) as! UILabel
        //let imgIcon : UIImageView = cell.contentView.viewWithTag(100) as! UIImageView //Temp Comments
        
        let menuObject = arrayMenuOptions[indexPath.row]
       // imgIcon.image = UIImage(named: menuObject.icon)
        lblTitle.text = menuObject.title
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let btn = UIButton(type: UIButton.ButtonType.custom)
        btn.tag = indexPath.row
        self.onCloseMenuClick(btn)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayMenuOptions.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
}

