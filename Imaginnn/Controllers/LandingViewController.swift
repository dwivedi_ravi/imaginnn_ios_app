//
//  LandingViewController.swift
//  Imaginnn
//
//  Created by Smruti Kanta Mohanty on 02/09/21.
//

import UIKit

enum TabBarItem: Int {
    case exploreIdeas = 1
    case myVault = 2
    case people = 3
    case favorite = 4
}

enum SEGUES: String {
    
    case exploreIdeaSegue  = "exploreIdeaSegue"
    case myVaultSegue      = "myVaultSegue"
    case peopleSegue       = "peopleSegue"
    case favoriteIdeaSegue = "favoriteIdeaSegue"
    case landingPageSegue  = "landingPageSegue"
    case changePinSegue    = "changePinSegue"
    case termsOfUseSegue   = "termsOfUseSegue"
    case rateTheAppSegue   = "rateTheAppSegue"
    case contactUsSegue    = "contactUsSegue"
    case faqsSegue         = "faqsSegue"
    case myProfileSegue    = "myProfileSegue"
    
}


class LandingViewController: BaseViewController {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var exploreIdeaBtn: UIButton!
    @IBOutlet weak var myVaultBtn: UIButton!
    @IBOutlet weak var peopleBtn: UIButton!
    @IBOutlet weak var favoriteBtn: UIButton!
    
    
    lazy var exploreIdeasVC: ExploreIdeasViewController = {
        let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        let viewController = storyboard?.instantiateViewController(withIdentifier: "exploreIdeaSegue") as! ExploreIdeasViewController
        self.addViewControllerAsChildViewController(viewController)
        return viewController
    }()
    
    lazy var myVaultViewController: MyVaultViewController = {
        let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        let viewController = storyboard?.instantiateViewController(withIdentifier: "myVaultSegue") as! MyVaultViewController
        self.addViewControllerAsChildViewController(viewController)
        return viewController
    }()
    
    lazy var peopleViewController: PeopleViewController = {
        let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        let viewController = storyboard?.instantiateViewController(withIdentifier: "peopleSegue") as! PeopleViewController
        self.addViewControllerAsChildViewController(viewController)
        return viewController
    }()
    
    lazy var favoriteIdeasViewController: FavoriteIdeasViewController = {
        let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        let viewController = storyboard?.instantiateViewController(withIdentifier: "favoriteIdeaSegue") as! FavoriteIdeasViewController
        self.addViewControllerAsChildViewController(viewController)
        return viewController
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        exploreIdeasVC.view.isHidden = false
        setSelectedThemeColor()
    }
    
    private func setSelectedThemeColor() {
        
        if let themeColor: UIColor = ImaginnnAppManager.shared.selectedColor {
            self.view.backgroundColor = themeColor
            self.exploreIdeaBtn = Helper.changeButtonImageColor("ic_explore_idea_tab", themeColor, exploreIdeaBtn)
            self.myVaultBtn = Helper.changeButtonImageColor("ic_myVault_tab", themeColor, myVaultBtn)
            self.peopleBtn = Helper.changeButtonImageColor("ic_people_tab", themeColor, peopleBtn)
            self.favoriteBtn = Helper.changeButtonImageColor("ic_favorite_tab", themeColor, favoriteBtn)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //MARK:- Helper Method for childViewController
    private func addViewControllerAsChildViewController(_ childViewController: UIViewController) {
        addChild(childViewController)
        containerView.addSubview(childViewController.view)
        childViewController.view.frame = self.containerView.bounds
        childViewController.didMove(toParent: self)
    }
    
    //MARK:- Helper To remove ChildViewController from ParentViewController
    private func removeViewControllerAsChildViewController(_ childViewController: UIViewController) {
        childViewController.willMove(toParent: nil)
        childViewController.view.removeFromSuperview()
        childViewController.removeFromParent()
    }
    
    private func hideAllViewControllers() {
        exploreIdeasVC.view.isHidden = true
        myVaultViewController.view.isHidden = true
        peopleViewController.view.isHidden = true
        favoriteIdeasViewController.view.isHidden = true
    }

    
    @IBAction func showSelectedViewController(_ sender: UIButton) {
        
        if (ImaginnnAppManager.shared.isLoding ?? false) || ImaginnnAppManager.shared.currentTab == sender.tag {
            return
        } else {
            hideAllViewControllers()
        }
        
        switch sender.tag {
        
        case TabBarItem.exploreIdeas.rawValue:
            ImaginnnAppManager.shared.currentTab = sender.tag
            exploreIdeasVC.view.isHidden = false
            exploreIdeasVC.viewWillAppear(true)
            
        case TabBarItem.myVault.rawValue:
            ImaginnnAppManager.shared.currentTab = sender.tag
            myVaultViewController.view.isHidden = false
            myVaultViewController.viewWillAppear(true)
            
        case TabBarItem.people.rawValue:
            ImaginnnAppManager.shared.currentTab = sender.tag
            peopleViewController.view.isHidden = false
            peopleViewController.viewWillAppear(true)
            
        case TabBarItem.favorite.rawValue:
            ImaginnnAppManager.shared.currentTab = sender.tag
            favoriteIdeasViewController.view.isHidden = false
            favoriteIdeasViewController.viewWillAppear(true)
            
        default:
            break
        }
    }
}
