//
//  MyVaultViewController.swift
//  Imaginnn
//
//  Created by Smruti Kanta Mohanty on 02/09/21.
//

import UIKit

class MyVaultViewController: BaseViewController {
    
    @IBOutlet weak var myVaultTableView: UITableView!
    @IBOutlet weak var menuBtn: UIButton!
    @IBOutlet weak var searchBtn: UIButton!
    @IBOutlet weak var addIdeaBtn: UIButton!
    @IBOutlet weak var searchBorderView: BMKCustomView!
    
    var isPageRefreshing: Bool = false
    var ideasCount: Int = 0
    var offsetCount: Int = 10

    var myVaultIdeaDataModel = MyVaultDataModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
     //   tempSetValue()
        addCustomUIButton(menuBtn)
        setPageUI()
        
    }
    
    func tempSetValue()  {
        ImaginnnAppManager.shared.selectedColor = ORANGE_COLOR
        ImaginnnAppManager.shared.userId  = "50"
        ImaginnnAppManager.shared.token   = "17b433b3-7180-4a8c-988b-31601ee7cd02"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        myVaultIdeaDataModel = MyVaultDataModel()
        myVaultIdeaDataModel.myVaultItems = [IdeaModel]()
        ideasCount = 0
        getExploreIdeasData()
    }
    
    private func setPageUI() {
        myVaultTableView.register(UINib(nibName: "CommonCellView", bundle: nil), forCellReuseIdentifier: "commonCell")
        setSelectedThemeColor()
        myVaultTableView.reloadData()
    }
    
    private func setSelectedThemeColor() {
        
        if let themeColor: UIColor = ImaginnnAppManager.shared.selectedColor {
            searchBorderView.borderColour = themeColor
            self.view.backgroundColor = themeColor
            self.searchBtn = Helper.changeButtonImageColor("ic_search", themeColor, searchBtn)
            self.addIdeaBtn = Helper.changeButtonImageColor("ic_add_icon", themeColor, addIdeaBtn)
        }
    }
    
    @IBAction func searchBtnPressed(_ sender: UIButton) {
        
    }
    
    @IBAction func addNewIdeaBtnPressed(_ sender: UIButton) {
        self.openViewControllerBasedOnIdentifier("createIdeaSegue")
    }
    
    
    private func getExploreIdeasData() {
     //   MBProgressHUD.showAdded(to: self.view, animated: true)
        ImaginnnAppManager.shared.isLoding = true
        if (myVaultIdeaDataModel.start_count != ideasCount) || ideasCount == 0 {
            myVaultIdeaDataModel.start_count = ideasCount
            myVaultIdeaDataModel.postMyVaultIdeaDataURL(onSuccess: {
                ImaginnnAppManager.shared.isLoding = false
                MBProgressHUD.hide(for: self.view, animated: true)
                self.ideasCount += self.offsetCount
                self.isPageRefreshing = false
                self.myVaultTableView.reloadData()
            }) { (error) in
                ImaginnnAppManager.shared.isLoding = false
                MBProgressHUD.hide(for: self.view, animated: true)
                Helper.showToastMessage(message: error?.localizedDescription ?? "", view: self.view)
            }
        }
     }
    
     
     func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
         let scrooViewPosition = (scrollView.contentSize.height - scrollView.frame.size.height) - scrollView.contentOffset.y
         
         if scrooViewPosition <= 50 {
             if !isPageRefreshing {
                 isPageRefreshing = true
                 getExploreIdeasData()
             }
         }
     }
}

extension MyVaultViewController: UITableViewDataSource, UITableViewDelegate  {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myVaultIdeaDataModel.myVaultItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "commonCell", for: indexPath) as? CommonTableViewCell else {
            return UITableViewCell()
        }
        
        let cellData = myVaultIdeaDataModel.myVaultItems[indexPath.row]
         cell.loadExploreIdeaData(cellData, baseUrl: myVaultIdeaDataModel.ideaBaseURL ?? "", true)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
}

