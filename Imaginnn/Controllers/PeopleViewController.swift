//
//  PeopleViewController.swift
//  Imaginnn
//
//  Created by Smruti Kanta Mohanty on 02/09/21.
//

import UIKit

class PeopleViewController: BaseViewController {

    
    @IBOutlet weak var menuBtn: UIButton!
    @IBOutlet weak var searchBtn: UIButton!
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var searchBorderView: BMKCustomView!
    @IBOutlet weak var peopleTableView: UITableView!
    
    var peopleDataModel = PeopleDataModel()
    var IsAPICallAllowed: Bool = true
    
    override func viewDidLoad() {
        addCustomUIButton(menuBtn)
        super.viewDidLoad()
       // tempSetValue()
        setSelectedThemeColor()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        peopleDataModel = PeopleDataModel()
        peopleDataModel.friendList = [FriendModel]()
        getFriendListData()
    }
    
    func tempSetValue()  {
        ImaginnnAppManager.shared.selectedColor = ORANGE_COLOR
        ImaginnnAppManager.shared.userId  = "50"
        ImaginnnAppManager.shared.token   = "17b433b3-7180-4a8c-988b-31601ee7cd02"
    }
    
    private func setSelectedThemeColor() {
        
        if let themeColor: UIColor = ImaginnnAppManager.shared.selectedColor {
            searchBorderView.borderColour = themeColor
            self.searchBtn = Helper.changeButtonImageColor("ic_search", themeColor, searchBtn)
            self.deleteBtn = Helper.changeButtonImageColor("ic_delete_icon", themeColor, deleteBtn)
        }
    }
    
    private func getFriendListData() {
        
       // MBProgressHUD.showAdded(to: self.view, animated: true)
        ImaginnnAppManager.shared.isLoding = true
        peopleDataModel.postPeolpleDataURL(onSuccess: {
            ImaginnnAppManager.shared.isLoding = false
            MBProgressHUD.hide(for: self.view, animated: true)
            self.peopleTableView.reloadData()
        }) { (error) in
            ImaginnnAppManager.shared.isLoding = false
            MBProgressHUD.hide(for: self.view, animated: true)
            Helper.showToastMessage(message: error?.localizedDescription ?? "", view: self.view)
        }
     }
    
    @IBAction func deletePeopleBtnPressed(_ sender: UIButton) {
        
    }
    
    @IBAction func searchBtnPressed(_ sender: UIButton) {
        
    }
    
}

extension PeopleViewController: UITableViewDataSource, UITableViewDelegate  {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return peopleDataModel.friendList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "peopleCell", for: indexPath) as? PeopleTableViewCell else {
            return UITableViewCell()
        }
        
        let cellData = peopleDataModel.friendList[indexPath.row]
        
        cell.loadFriendDataOnCell(with: cellData, baseURL: peopleDataModel.userProfileURL ?? "")
                
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
}


