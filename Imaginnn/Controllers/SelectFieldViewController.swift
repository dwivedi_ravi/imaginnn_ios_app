//
//  SelectFieldViewController.swift
//  Imaginnn
//
//  Created by Bharat Khatke on 23/09/21.
//

import UIKit

protocol SelectFieldDelegate {
    func didDoneBtnPressed(data: [CategoryModel])
}

class SelectFieldViewController: UIViewController {
    
    @IBOutlet weak var selectFieldTableView: UITableView!

    var selectFieldDelegate: SelectFieldDelegate?
    var fieldArray = [CategoryModel]()
    
    var selectedBtnCount: Int      = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        selectFieldTableView.reloadData()
    }
    
    @IBAction func cancelBtnPressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func doneBtnPressed(_ sender: UIButton) {
        self.dismiss(animated: true) {
            let selectedCategory = self.fieldArray.filter { $0.isSelected == true }
            self.selectFieldDelegate?.didDoneBtnPressed(data: selectedCategory)
        }
        
    }
    
    @objc func checkBoxButtomPressed(_ sender: UIButton) {
        
        let selectedCategory = self.fieldArray.filter { $0.isSelected == true }
        
        selectedBtnCount = selectedCategory.count
        
        if self.fieldArray[sender.tag].isSelected ?? false {
            if selectedBtnCount <= 3 {
                self.fieldArray[sender.tag].isSelected = false
                selectedBtnCount -= 1
            }
        } else {
            if selectedBtnCount < 3 {
                self.fieldArray[sender.tag].isSelected = true
                selectedBtnCount += 1
            } else {
                Helper.showToastMessage(message: MacroConstant.MAX_CATEGORY_ITEM_MSG, view: self.view)
            }
        }
        let indexPath = IndexPath(row: sender.tag, section: 0)
        self.selectFieldTableView.reloadRows(at: [indexPath], with: .automatic)
    }
}

extension SelectFieldViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.fieldArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "selectFieldCell", for: indexPath) as? SelectFieldTableCell else {
            return UITableViewCell()
        }
        
        cell.checkBox.tag = indexPath.row
        let fieldData = self.fieldArray[indexPath.row]
        
        cell.loadSelectFieldCell(cellData: fieldData)
        cell.checkBox.addTarget(self, action: #selector(checkBoxButtomPressed(_ :)), for: .touchUpInside)
        return cell
    }
}
