//
//  InitialViewModel.swift
//  Imaginnn
//
//  Created by Smruti Kanta Mohanty on 31/08/21.
//

import Foundation


struct InitialViewModel {
    
    var setColorModelList: [ColorItem]
    
    init() {
        setColorModelList = [ColorItem]()
        setColorModelList = defaultColorSet()
    }
    
    private func defaultColorSet() -> [ColorItem] {
        let item1 = ColorItem(color: RED_COLOR, index: 0, colorName: "red")
        let item2 = ColorItem(color: BLUE_COLOR, index: 1, colorName: "blue")
        let item3 = ColorItem(color: ORANGE_COLOR, index: 2, colorName: "orange")
        let item4 = ColorItem(color: NAVI_BLUE_COLOR, index: 3, colorName: "naviBlue")
        let item5 = ColorItem(color: YELLOW_COLOR, index: 4, colorName: "yellow")
        let item6 = ColorItem(color: SKY_BLUE_COLOR, index: 5, colorName: "skyBlue")
        let item7 = ColorItem(color: GREEN_COLOR, index: 6, colorName: "green")
        let item8 = ColorItem(color: PURPLE_COLOR, index: 7, colorName: "purple")
        return [item1, item2, item3, item4, item5, item6, item7, item8]
    }
    
    func numberOfRowsInSection() -> Int {
        return self.setColorModelList.count
    }
    
    func cellItemAtIndex(_ index: Int) -> ColorItem {
        let cellData = self.setColorModelList[index]
        return cellData
    }
}

struct ColorItem {
    var color: UIColor
    var index: Int
    var colorName: String
}
