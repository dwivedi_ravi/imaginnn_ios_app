//
//  AppDelegate.swift
//  Imaginnn
//
//  Created by Bharat Khatke on 28/08/21.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift
import Firebase
import FirebaseMessaging
import UserNotifications


let appDelegate = UIApplication.shared.delegate as? AppDelegate

//@main
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {
    
    var window: UIWindow?
    let gcmMessageIDKey = "gcm.message_id"
    


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        IQKeyboardManager.shared.enable = true
        FirebaseApp.configure()
        
        clearKeychainIfWillUnistall()
        
        //Notification Configuration
        if #available(iOS 10.0, *) {
          // For iOS 10 display notification (sent via APNS)
          UNUserNotificationCenter.current().delegate = self

          let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
          UNUserNotificationCenter.current().requestAuthorization(
            options: authOptions,
            completionHandler: {_, _ in })
        } else {
          let settings: UIUserNotificationSettings =
          UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
          application.registerUserNotificationSettings(settings)
        }

        application.registerForRemoteNotifications()
        Messaging.messaging().delegate = self
        
        return true
    }
    
    //MARK:- Reset Key chain data here
    func clearKeychainIfWillUnistall() {

        let freshInstall = !UserDefaults.standard.bool(forKey: "alreadyInstalled")
        if freshInstall {
            keyChainWrapper()
            UserDefaults.standard.set(true, forKey: "alreadyInstalled")
        }
    }
    
    private func keyChainWrapper() {
        let keychainItemWrapper = KeychainItemWrapper(identifier: APP_IDENTIFIER, accessGroup: nil)
        keychainItemWrapper.resetKeychain()
    }
    
     func logOutUser() {
        keyChainWrapper()
        UpdateAppData.resetUserData()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let destVC = storyboard.instantiateViewController(withIdentifier: "initaiViewSegue") as! InitialViewController
        self.window?.rootViewController = destVC
    }
    
    private func gotoRespectedPage() {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        
        if let userID = AppUtility.getUserDetailsBy(key: .userId), !userID.isEmpty {
            UpdateAppData.setSavedUserData()
            let vc = storyBoard.instantiateViewController(withIdentifier: "loginSegue") as! LoginViewController
           self.window?.rootViewController = vc
        }
    }
    
    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {

        let container = NSPersistentContainer(name: "Imaginnn")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        
        if Auth.auth().canHandle(url) {
            return true
        }
        
        return false
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
        print("Device token: \(deviceToken)")
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
      // Print message ID.
      if let messageID = userInfo[gcmMessageIDKey] {
        print("Message ID1: \(messageID)")
      }
      print(userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
      
      // Print message ID.
      if let messageID = userInfo[gcmMessageIDKey] {
        print("Message ID2: \(messageID)")
      }

      // Print full message.
      print(userInfo)
        
      if Auth.auth().canHandleNotification(userInfo) {
        completionHandler(.noData)
        return
      }
        
        
      completionHandler(.newData)
    }
    
    @available(iOS 10, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
      withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
      let userInfo = notification.request.content.userInfo

      // Print message ID.
      if let messageID = userInfo[gcmMessageIDKey] {
        print("Message ID3: \(messageID)")
      }

      // Print full message.
      print(userInfo)

      // Change this to your preferred presentation option
      completionHandler([.alert, .sound])
    }

    @available(iOS 10, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
      let userInfo = response.notification.request.content.userInfo
      // Print message ID.
      if let messageID = userInfo[gcmMessageIDKey] {
        print("Message ID4: \(messageID)")
      }

      // Print full message.
      print(userInfo)
        
      //Handle deep link if present else normal flow
      if let deepLink = userInfo["actionURL"] as? String {
        print(deepLink)
      }

      completionHandler()
    }
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
    
        if OrientationManager.landscapeSupported {
           // return .allButUpsideDown
            return .landscape
        }
        return .portrait
    }

}

extension AppDelegate: MessagingDelegate {
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        print("Firebase registration token: \(String(describing: fcmToken))")
        if let deviceToken = fcmToken {
            DEVICE_TOKEN = deviceToken
            let dataDict:[String: String] = ["token": deviceToken]
            NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        }
    }
}
