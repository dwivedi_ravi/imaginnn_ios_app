//
//  BMKCheckBox.swift
//  Imaginnn
//
//  Created by Bharat Khatke on 23/09/21.
//

import Foundation

class BMKCheckBox: UIButton {

    // Images
    @IBInspectable var onImage: UIImage = UIImage()
    
    @IBInspectable var offImage:UIImage   =  UIImage()
  
    @IBInspectable var isCheckbox : Bool = false {
        didSet{
            if isCheckbox == true {
                self.setImage(onImage, for: .normal)
            } else {
                self.setImage(offImage, for: .normal)
            }
        }
    }
    
    
    override func awakeFromNib() {
        self.addTarget(self, action:#selector(buttonClicked(sender:)), for: .touchUpInside)
    }
    
    @objc func buttonClicked(sender: UIButton) {
        if sender == self {
            isCheckbox = !isCheckbox
        }
    }
}
