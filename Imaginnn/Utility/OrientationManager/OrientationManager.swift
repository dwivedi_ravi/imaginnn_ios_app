//
//  OrientationManager.swift
//  Imaginnn
//
//  Created by Bharat Khatke on 29/08/21.
//

import UIKit

class OrientationManager {
    //The code below will automatically rotate your device's orientation when you exit this ViewController
    static var landscapeSupported: Bool = false
    static func lockOrientation(_ orientation: UIInterfaceOrientation, isLandScape: Bool = false) {
        landscapeSupported = isLandScape
        UIDevice.current.setValue(orientation.rawValue, forKey: "orientation")
    }
}
