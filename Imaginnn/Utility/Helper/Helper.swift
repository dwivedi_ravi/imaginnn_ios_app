//
//  Helper.swift
//  Imaginnn
//
//  Created by Bharat Khatke on 29/08/21.
//

import UIKit


struct Helper {
    
    
    //MARK:- Show Toast Message
    static func showToastMessage(message: String, view: UIView, color: UIColor = .darkGray, duration: Double = 3.0) {
        UIView.hr_setToastThemeColor(color: color)
        view.makeToast(message: message, duration: duration, position: HRToastPositionDefault as AnyObject)
    }
    
    
    // MARK:- Hex sytring to UIColor
    static func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    //MARK:- Convert String to Dict
    static func convertStringToDictionary(text: String) -> [String:AnyObject]? {
        if let data = text.data(using: String.Encoding.utf8) {
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject]
                return json
            } catch {}
        }
        return nil
    }
    
    //MARK:- Change Buttom image Color
    static func changeButtonImageColor(_ imageName: String, _ color: UIColor, _ button: UIButton) -> UIButton {
        let originalImg = UIImage(named: imageName)
        let tintedImg = originalImg?.withRenderingMode(.alwaysTemplate)
        button.setImage(tintedImg, for: .normal)
        button.tintColor = color
        return button
    }
    
    //MARK:- Get App Bundle and build Version
    static func getAppBundleVersion() -> String {
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
          return version
        }
        return ""
    }
    
    static func removeLastCharaterFromString(_ string: String) -> String {
        return String(string.dropLast())
    }
    
    //MARK:- Error from error message
    static func error(withLocalizedDescription localizedDescription: String?) -> Error? {
        let userInfo = [NSLocalizedDescriptionKey: localizedDescription ?? "Error"]
        return NSError(domain: MFDemoErrorDomain, code: MFDemoErrorCode, userInfo: userInfo)
    }
    
    //MARK:- get Number of dayes from date
    static func getNumberOfDays(dateStr: String) -> Int {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let currentDate = Date()
        if let endDate = dateFormatter.date(from: dateStr) {
            
            return currentDate.days(from: endDate)
        }
        
        return 0
    }
    
    //MARK:- Get array from string seperated by character
    static func getSplittingStringArrayBy(_ splittingString: String, _ separateChar: String) -> [String]? {
        
        return splittingString.components(separatedBy: separateChar)
    }
    
    //MARK:- Get original Image
    static func getOriginalImage(image: UIImage) -> UIImage? {
        
        UIGraphicsBeginImageContextWithOptions((image.size), false, (image.scale))
        image.draw(in: CGRect(origin: .zero, size: image.size))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
}

extension String {
    func replace(string:String, replacement:String) -> String {
        return self.replacingOccurrences(of: string, with: replacement, options: NSString.CompareOptions.literal, range: nil)
    }

    func removeWhitespace() -> String {
        return self.replace(string: " ", replacement: "")
    }
}

extension UIButton {
    func underline(color: UIColor) {
        guard let text = self.titleLabel?.text else { return }
        
        let attributedString = NSMutableAttributedString(string: text)
        
        attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location: 0, length: text.count))
        
    }
}

extension UIImage {
    enum JPEGQuality: CGFloat {
        case lowest  = 0
        case low     = 0.25
        case medium  = 0.5
        case high    = 0.75
        case highest = 1
    }
    
    /// Returns the data for the specified image in JPEG format.
    /// If the image object’s underlying image data has been purged, calling this function forces that data to be reloaded into memory.
    /// - returns: A data object containing the JPEG data, or nil if there was a problem generating the data. This function may return nil if the image has no data or if the underlying CGImageRef contains data in an unsupported bitmap format.
    
    func jpeg(_ jpegQuality: JPEGQuality) -> Data? {
        return jpegData(compressionQuality: jpegQuality.rawValue)
    }
    
    func crop(to:CGSize) -> UIImage {

      guard let cgimage = self.cgImage else { return self }

      let contextImage: UIImage = UIImage(cgImage: cgimage)

      guard let newCgImage = contextImage.cgImage else { return self }

      let contextSize: CGSize = contextImage.size

      //Set to square
      var posX: CGFloat = 0.0
      var posY: CGFloat = 0.0
      let cropAspect: CGFloat = to.width / to.height

      var cropWidth: CGFloat = to.width
      var cropHeight: CGFloat = to.height

      if to.width > to.height { //Landscape
          cropWidth = contextSize.width
          cropHeight = contextSize.width / cropAspect
          posY = (contextSize.height - cropHeight) / 2
      } else if to.width < to.height { //Portrait
          cropHeight = contextSize.height
          cropWidth = contextSize.height * cropAspect
          posX = (contextSize.width - cropWidth) / 2
      } else { //Square
          if contextSize.width >= contextSize.height { //Square on landscape (or square)
              cropHeight = contextSize.height
              cropWidth = contextSize.height * cropAspect
              posX = (contextSize.width - cropWidth) / 2
          }else{ //Square on portrait
              cropWidth = contextSize.width
              cropHeight = contextSize.width / cropAspect
              posY = (contextSize.height - cropHeight) / 2
          }
      }

      let rect: CGRect = CGRect(x: posX, y: posY, width: cropWidth, height: cropHeight)

      // Create bitmap image from context using the rect
      guard let imageRef: CGImage = newCgImage.cropping(to: rect) else { return self}

      // Create a new image based on the imageRef and rotate back to the original orientation
      let cropped: UIImage = UIImage(cgImage: imageRef, scale: self.scale, orientation: self.imageOrientation)

      UIGraphicsBeginImageContextWithOptions(to, false, self.scale)
      cropped.draw(in: CGRect(x: 0, y: 0, width: to.width, height: to.height))
      let resized = UIGraphicsGetImageFromCurrentImageContext()
      UIGraphicsEndImageContext()

      return resized ?? self
    }
    
    public class func gif(asset: String) -> UIImage? {
      if let asset = NSDataAsset(name: asset) {
        // return UIImage.gif(data: asset.data)
        return UIImage.gif(asset: asset.name)
      }
      return nil
    }
}
