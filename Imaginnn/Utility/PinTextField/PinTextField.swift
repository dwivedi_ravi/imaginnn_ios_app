//
//  PinTextField.swift
//  Imaginnn
//
//  Created by Smruti Kanta Mohanty on 01/09/21.
//

import UIKit

protocol PinTexFieldDelegate : UITextFieldDelegate {
    func didPressBackspace(textField : PinTextField)
}

class PinTextField: UITextField {
    
    override func deleteBackward() {
        super.deleteBackward()
        // If conforming to our extension protocol
        if let pinDelegate = self.delegate as? PinTexFieldDelegate {
            pinDelegate.didPressBackspace(textField: self)
        }
    }
}
