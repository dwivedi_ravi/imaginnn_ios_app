//
//  BMKCustomView.swift
//  Imaginnn
//
//  Created by Bharat Khatke on 29/08/21.
//

import UIKit

@available(iOS 11.0, *)
@IBDesignable class BMKCustomView: UIView {
    
    public override init(frame: CGRect)
    {
        super.init(frame: frame)
    }
    
    public required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
    }
    
    @IBInspectable
    public var cornerRadius: CGFloat = 0.0
    {
        didSet
        {
          //  self.clipsToBounds = true
            self.layer.cornerRadius = self.cornerRadius
        }
    }
    
    @IBInspectable
    public var cornerSide: String = ""
    {
        didSet
        {
          //  self.clipsToBounds = true
            if cornerSide == "topLeft" {
              self.layer.maskedCorners = [.layerMinXMinYCorner]
            }
            else if cornerSide == "topRight" {
                self.layer.maskedCorners = [.layerMaxXMinYCorner]
            }
            else if cornerSide == "bottomLeft" {
                self.layer.maskedCorners = [.layerMinXMaxYCorner]
            }
            else if cornerSide == "bottomRight" {
                self.layer.maskedCorners = [.layerMaxXMaxYCorner]
            }
            else if cornerSide == "top" {
                self.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
            }
            else if cornerSide == "bottom" {
                self.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
            }
            else if cornerSide == "left" {
                self.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner]
            }
            else if cornerSide == "right" {
                self.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner]   //.layerMaxXMinYCorner, .layerMinXMaxYCorner
            }
            else if cornerSide == "3BottomLeft" {
                self.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner, .layerMaxXMaxYCorner]
            }
            else if cornerSide == "3BottomRight" {
                self.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner, .layerMinXMaxYCorner]
            }
            else if cornerSide == "3TopLeft" {
                self.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner, .layerMaxXMinYCorner]
            }
            else if cornerSide == "3TopRight" {
                self.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMaxYCorner, .layerMinXMaxYCorner]
            }else{
                self.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner, .layerMinXMinYCorner, .layerMaxXMinYCorner]
            }
        }
        
        // .layerMaxXMaxYCorner bottom right
        // .layerMinXMaxYCorner bottom left
        // .layerMinXMinYCorner Top Left
        // .layerMaxXMinYCorner Top Right
        
    }
    
    
    @IBInspectable
    public var borderWidth: CGFloat = 0.0
    {
        didSet
        {
            self.layer.borderWidth = self.borderWidth
        }
    }
    
    @IBInspectable
    public var borderColour: UIColor = UIColor.clear
    {
        didSet
        {
            self.layer.borderColor = self.borderColour.cgColor
        }
    }
    
    @IBInspectable
    public var shadowColour: UIColor = UIColor.clear
    {
        didSet
        {
            self.layer.shadowColor = self.shadowColour.cgColor
            self.layer.shadowOffset = CGSize(width: 1, height: 1)
          //  self.layer.shadowOffset = CGSize.zero
          //  self.layer.shouldRasterize = true
        }
    }
    
    @IBInspectable
    public var shadowRadius: CGFloat = 0.0
    {
        didSet
        {
            self.layer.shadowRadius = self.shadowRadius
        }
    }
    
    @IBInspectable
    public var shadowOpacity: Float = 0.0
    {
        didSet
        {
            self.layer.shadowOpacity = self.shadowOpacity
        }
    }
    
}
