//
//  GlobalValidation.swift
//  Imaginnn
//
//  Created by Bharat Khatke on 05/09/21.
//


import UIKit

let customStringOnly = "^[a-zA-Z0-9,/: ]*$"
let alphabeticStringWithSpace = "^[a-zA-Z ]*$"
let OnlyNumbers = "^[0-9]*$"
let OnlyAlphabetsWithoutSpaces = "[^a-z|^A-Z]"
let Name = "^(([a-z]+['-\\.,]??[a-z]+)|([a-z]+[,\\.]??))$"
let alphanumeric = "^[ A-Za-z0-9]*$"
let email = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
let onlySpecialcharacter = "^[!@#$&()\\-`.+,/\"]*$"
let Min8CharOneLetOneNumber = "^(?=.*[A-Za-z])(?=.*)(?=.*[$@$!%*#?&])[A-Za-z$@$!%*#?&]{1,}$"

let alphanumericMandate = "([A-Za-z]+[0-9]|[0-9]+[A-Za-z])[A-Za-z0-9]*"

let MFDemoErrorDomain = "MFDemoErrorDomain"
let MFDemoErrorCode : Int = 100

enum Alert {
    case success
    case failure
    case error
}

enum Valid {
    case success
    case failure(Alert, AlertMessages)
}

enum ValidationType {
    case email
    case stringWithFirstLetterCaps
    case phoneNo
    case alphabeticString
    case userID
    case password
    case userName
}

enum RegEx: String {
    case email = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
    case password = "^[a-zA-Z0-9_!@#$&.,+()-]{6,15}$" // Password length 6-15
    case alphabeticStringWithSpace = "^[a-zA-Z ]*$" // e.g. hello sandeep
    case alphabeticStringFirstLetterCaps = "^[A-Z]+[a-zA-Z]*$" // SandsHell
    case phoneNo =  "[0-9]{10}$"  // PhoneNo 10-14 Digits
    case alphanumeric = "^[ A-Za-z0-9]*$"
    case nameWithAlphanumeric = "^(([A-Za-z0-9]+['-\\.,]??[a-z]+)|([a-z]+[,\\.]??))$"
    case Name = "^(([a-z]+['-\\.,]??[a-z]+)|([a-z]+[,\\.]??))$"
    case OnlyNumbers = "^[0-9]*$"
    case OnlyAlphabetsWithoutSpaces = "[^a-z|^A-Z]"
    case OnlySpecialCharacter = "^[!@#$&()\\-`.+,/\"]*$"
    case Min8CharOneLetOneNumber = "^(?=.*[A-Za-z])(?=.*)(?=.*[$@$!%*#?&])[A-Za-z$@$!%*#?&]{8,}$"
    case Multiple = "^[1-9]+[0-9]*00$"
    
    //Change RegEx according to your Requirement
}
//
enum AlertMessages: String {
    case inValidEmail            = "Email ID format should be: name@example.com"
    case invalidFirstLetterCaps  = "First Letter should be capital"
    case inValidPhone            = "Please enter a valid 10-digit mobile number"
    case invalidAlphabeticString = "Invalid String"
    case inValidPSW              = "Wrong Password"
    
    case emptyPhone              = "Mobile Number cannot be left blank"
    case emptyEmail              = "Email ID cannot be left blank"
    case emptyFirstLetterCaps    = "Empty Name"
    case emptyAlphabeticString   = "Empty String"
    case emptyRegistrationName   = "Please enter a valid name"
    
    case emptyUserID             = "Empty UserID"
    case emptyPSW                = "Password cannot be left blank"
    case inValidUserID           = "Invalid UserID"
    case userName                = "Enter your last name"
    case emptyUserName           = "Empty User name"
    case emptyEmailOrMobileNo    = "Email ID or Mobile cannot be left blank"
    case invalidFullName         = "Full Name is invalid" //"Full Name should contain first name and last name"
   
    func localized() -> String {
        return NSLocalizedString(self.rawValue, comment: "")
    }
}

extension NSString {
    
    func validateStringWithPattern(pattern: String) -> Bool {
       
        let regex = try? NSRegularExpression(pattern: pattern, options: .caseInsensitive)
        assert(regex != nil, "Unable to create regular expression")
        let textRange = NSRange(location: 0, length: self.length)
        let matchRange: NSRange? = regex?.rangeOfFirstMatch(in: self as String, options: .reportProgress, range: textRange)
        
        var didValidate = false
        // Did we find a matching range
        if matchRange?.location != NSNotFound {
            didValidate = true
        }
        
        return didValidate
        
    }
    
    func filterExtraWhitespaces() -> String? {
        let whitespaces = CharacterSet.whitespaces
        let noEmptyStrings = NSPredicate(format: "SELF != ''")
        let parts = components(separatedBy: whitespaces)
        let filteredArray = (parts as NSArray).filtered(using: noEmptyStrings) as NSArray
         let filteredString = filteredArray.componentsJoined(by: " ")
  
        return filteredString
    }
    
    var isValidDouble: Bool
    {
        return Double(self.trimmingCharacters(in: .whitespaces)) != nil
        
    }
    
    var isValidInteger : Bool {
        return Int(self.trimmingCharacters(in: .whitespaces)) != nil
    }
    
    var isValidHexaDecimal: Bool {
      _ = UInt(self as String, radix: 16)!
           return true
    }
    
    func containCustomString() -> Bool {
        return self.validateStringWithPattern(pattern: customStringOnly)
    }
    
    func containsAlphabets() -> Bool {
        return self.validateStringWithPattern(pattern: alphabeticStringWithSpace)
    }
    
    func containOneLetterAndOneNumber() -> Bool {
         return self.validateStringWithPattern(pattern: alphanumericMandate)
    }
    

    func containsSpacesInBetween() -> Bool {
        let trimmedString = filterExtraWhitespaces()
        let whiteSpaceRange: NSRange = (trimmedString! as NSString).rangeOfCharacter(from: CharacterSet.whitespaces)
        if Int(whiteSpaceRange.location) != NSNotFound {
            return true
        } else {
            return false
        }
    }
    
    func containsOnlyNumbers() -> Bool {
        return self.validateStringWithPattern(pattern: OnlyNumbers)
    }
    
    func containsOnlyAlphabetsWithoutSpaces() -> Bool {
        return self.validateStringWithPattern(pattern: OnlyAlphabetsWithoutSpaces)
    }
    
    func containsOnlySpaces() -> Bool {
        var isValid = false
        
        let rawString = self
        let whiteSpace = CharacterSet.whitespacesAndNewlines
        let trimmed = rawString.trimmingCharacters(in: whiteSpace)
        
        if trimmed.count == 0 {
            isValid = true
        }else{
            isValid = false
        }
        
        return isValid
    }
    
    func containsValidEmail() -> Bool {
        return self.validateStringWithPattern(pattern: email)
    }
}


class GlobalValidation: NSObject {
    
    public static let shared = GlobalValidation()
    
    func validate(values: (type: ValidationType, inputValue: String)...) -> Valid {
        for valueToBeChecked in values {
            switch valueToBeChecked.type {
            case .email:
                if let tempValue = isValidString((valueToBeChecked.inputValue, .email, .emptyEmail, .inValidEmail)) {
                    return tempValue
                }
            case .stringWithFirstLetterCaps:
                if let tempValue = isValidString((valueToBeChecked.inputValue, .alphabeticStringFirstLetterCaps, .emptyFirstLetterCaps, .invalidFirstLetterCaps)) {
                    return tempValue
                }
            case .phoneNo:
                if let tempValue = isValidString((valueToBeChecked.inputValue, .phoneNo, .emptyPhone, .inValidPhone)) {
                    return tempValue
                }
            case .alphabeticString:
                if let tempValue = isValidString((valueToBeChecked.inputValue, .alphabeticStringWithSpace, .emptyAlphabeticString, .invalidAlphabeticString)) {
                    return tempValue
                }
            case .password:
                if let tempValue = isValidString((valueToBeChecked.inputValue, .password, .emptyPSW, .inValidPSW)) {
                    return tempValue
                }
            case .userID:
                if let tempValue = isValidString((valueToBeChecked.inputValue, .alphanumeric, .emptyUserID, .inValidUserID)) {
                    return tempValue
                }
            case .userName:
                if let tempValue = isValidString((valueToBeChecked.inputValue, .alphabeticStringWithSpace, .emptyUserName, .userName)) {
                    return tempValue
                }
            
            }
        }
        return .success
    }
    
    func isValidString(_ input: (text: String, regex: RegEx, emptyAlert: AlertMessages, invalidAlert: AlertMessages)) -> Valid? {
        if input.text.isEmpty {
            return .failure(.error, input.emptyAlert)
        } else if isValidRegEx(input.text, input.regex) != true {
            return .failure(.error, input.invalidAlert)
        }
        return nil
    }
    
    func isValidRegEx(_ testStr: String, _ regex: RegEx) -> Bool {
        let stringTest = NSPredicate(format:"SELF MATCHES %@", regex.rawValue)
        let result = stringTest.evaluate(with: testStr)
        return result
    }

}
