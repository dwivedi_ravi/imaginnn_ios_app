//
//  ImaginnnAppManager.swift
//  Imaginnn
//
//  Created by Bharat Khatke on 01/09/21.
//

import Foundation

class ImaginnnAppManager {
    
    static let shared = ImaginnnAppManager()
    
    private init() {
    }
    
    var selectedColor: UIColor?
    var email: String?
    var mobile_no: String?
    var token: String?
    var userId: String?
    var username: String?
    var isUserLoged: Bool?
    var isLoding: Bool?
    var currentTab: Int?
    var pin: String?
}


enum ImaginnnThemeColor: String {
    case red = "red"
    case blue = "blue"
    case orange = "orange"
    case naviBlue = "naviBlue"
    case yellow = "yellow"
    case skyBlue = "skyBlue"
    case green = "green"
    case purple = "purple"
}

struct ThemeColor {
    static func getThemeColor(_ colorName: ImaginnnThemeColor) -> UIColor {
        switch colorName {
        case .red:
            return RED_COLOR
        case .blue:
            return BLUE_COLOR
        case .orange:
            return ORANGE_COLOR
        case .naviBlue:
            return NAVI_BLUE_COLOR
        case .yellow:
            return YELLOW_COLOR
        case .skyBlue:
            return SKY_BLUE_COLOR
        case .green:
            return GREEN_COLOR
        case .purple:
            return PURPLE_COLOR
        }
    }
}


struct UpdateAppData {
    
    static func setSavedUserData() {
        
        if let userID = AppUtility.getUserDetailsBy(key: .userId), !userID.isEmpty {
            ImaginnnAppManager.shared.username = AppUtility.getUserDetailsBy(key: .username)
            ImaginnnAppManager.shared.email = AppUtility.getUserDetailsBy(key: .email)
            ImaginnnAppManager.shared.mobile_no = AppUtility.getUserDetailsBy(key: .mobile_no)
            ImaginnnAppManager.shared.token = AppUtility.getUserDetailsBy(key: .token)
            ImaginnnAppManager.shared.userId = AppUtility.getUserDetailsBy(key: .userId)
            ImaginnnAppManager.shared.pin   = AppUtility.getUserDetailsBy(key: .pin)
        }
        
    }
    
    static func resetUserData() {
        ImaginnnAppManager.shared.username  = nil
        ImaginnnAppManager.shared.email     = nil
        ImaginnnAppManager.shared.mobile_no = nil
        ImaginnnAppManager.shared.token     = nil
        ImaginnnAppManager.shared.userId    = nil
        ImaginnnAppManager.shared.pin       = nil
    }
}
