//
//  RegisterModel.swift
//  Imaginnn
//
//  Created by Bharat Khatke on 04/09/21.
//

import Foundation
import ObjectMapper

struct RegisterModel {
    
    static func validateUserDetails(_ userName: String, _ email: String, _ mobileNo: String) -> Error? {
        let bmkWrapper = BMKWrapper()
        
        var errorEmail: Error?
        var errorMobileNo: Error?
        
        let isEmailID = GlobalValidation.shared.validate(values:  (ValidationType.email, email))
        switch isEmailID {
        case .success:
            break
        case .failure(_, let message):
        
            errorEmail =  Helper.error(withLocalizedDescription: message.localized())
            break
        }
        
        
        let isMobileNo = GlobalValidation.shared.validate(values:  (ValidationType.phoneNo, mobileNo))
        switch isMobileNo {
        case .success:
            
            break
        case .failure(_, let message):
            errorMobileNo = Helper.error(withLocalizedDescription: message.localized())
            break
        }
        
        
        
        if userName.isEmpty {
            
            return bmkWrapper.error(withMessage: MacroConstant.EMPTY_USER_NAME_ERROR, errorCode: 0)
            
        } else if (errorEmail != nil) {
            
            return errorEmail
            
        } else if (errorMobileNo != nil) {
            
            return errorMobileNo
            
        }
        
        return nil
    }
}

struct RegisterUserModel: Mappable {
    var category: String?
    var remark: String?
    var result: Bool?
    var username: String?
    var resCode: String?
    var userId: String?
    var code: String?
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        category <- map["category"]
        remark <- map["remark"]
        result <- map["result"]
        username <- map["username"]
        resCode <- map["resCode"]
        userId <- map["userId"]
        code <- map["code"]
    }
}
