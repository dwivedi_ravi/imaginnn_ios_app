//
//  CreateIdeaDataModel.swift
//  Imaginnn
//
//  Created by Bharat Khatke on 02/10/21.
//

import Foundation
import ObjectMapper

class CreateIdeaDataModel: BMKWrapper {
    
    var categoryItems = [CategoryModel]()
    var createIdeaParam: CreateIdeaAPIParam?
    //MARK:- Category Data
    func getCategoryIdeaListDataURL(onSuccess: @escaping () -> Void, onFailure: @escaping (_ error: Error?) -> Void) {
        
        self.categoryItems = [CategoryModel]()
        
        self.GET(GET_CATEGORY_LIST, parameters: nil, success: { (response) in
            if let array = response as? Array<Any> {
               
                for arr in array {
                    if var model = Mapper<CategoryModel>().map(JSON: arr as! [String : Any]) {
                        model.isSelected = false
                        self.categoryItems.append(model)
                    }
                }
            }
            onSuccess()
        }) { (error) in
            onFailure(error)
        }
    }
    
    func postCreateIdeaDataURL(onSuccess: @escaping () -> Void, onFailure: @escaping (_ error: Error?) -> Void) {
        
        guard let paramObject = self.createIdeaParam else { return }
        
        let param = ["title" : paramObject.title, "caption" : paramObject.caption, "category_id" : paramObject.category_id, "userId" : paramObject.userId]
        
        print(param)
        
        self.postData(POST_CREATE_IDEA_URL, parameters: param, success: { responseData in
            print(responseData as Any)
            if  responseData != nil  {
                var responseCode = ""
                var errorMessage = ""
                if let data = responseData as? NSDictionary {
                    responseCode = (data.value(forKey: "code")) as! String
                    errorMessage = data.value(forKey: "remark") as! String
                }
                
                if responseCode != "200" {
                    onFailure(Helper.error(withLocalizedDescription: errorMessage))
                } else {
                    onSuccess()
                }
                
            } else{
                onFailure(Helper.error(withLocalizedDescription: MacroConstant.INVALID_JSON_OBJECT))
            }
        }) { (error) in
            print(error?.localizedDescription as Any)
            onFailure(error)
        }
    }
    
}

struct CreateIdeaAPIParam {
    var title: String
    var caption: String
    var category_id: String
    var userId: String
    init(_ title: String, _ caption: String, _ category_id: String, _ userId: String) {
        self.title = title
        self.caption = caption
        self.category_id = category_id
        self.userId = userId
    }
}


struct CategoryModel: Mappable {
    var categoryDesc: String?
    var categoryId: Int?
    var categoryImage: String?
    var categoryName: String?
    var categoryStatus: String?
    var orderNo: Int?
    var isSelected: Bool?
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        categoryDesc <- map["categoryDesc"]
        categoryId <- map["categoryId"]
        categoryImage <- map["categoryImage"]
        categoryName <- map["categoryName"]
        categoryStatus <- map["categoryStatus"]
        orderNo <- map["orderNo"]
        isSelected <- map["isSelected"]
    }
}
