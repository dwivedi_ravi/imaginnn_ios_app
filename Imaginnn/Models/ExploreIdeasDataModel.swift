//
//  ExploreIdeasDataModel.swift
//  Imaginnn
//
//  Created by Smruti Kanta Mohanty on 06/09/21.
//

import Foundation
import ObjectMapper


class ExploreIdeasDataModel: BMKWrapper {
    
    var start_count: Int = 0
    var offset_count: Int = 10
    var filter: String?
    var exploreIdeaModel: ExploreIdeaModel?
    var ideaBaseURL: String?
    var ideaItems = [IdeaModel]()
    var isCallAPI: Bool = false
    
    
    //MARK:- Explore Data
    
    func postExploreIdeaDataURL(onSuccess: @escaping (_ responseCount: Int) -> Void, onFailure: @escaping (_ error: Error?) -> Void) {
        
        let userID = ImaginnnAppManager.shared.userId ?? ""
        let param = ["userId" : userID, "start_count" : "\(start_count)", "offset_count" : "\(offset_count)", "filter" : filter ?? "time"] as [String : Any]
       
        print(param)
        
       
        self.postData(POST_EXPLORE_IDEAS, parameters: param, success: { responseData in
            print(responseData as Any)
            var responseCount: Int = 0
            if  responseData != nil  {
                
                self.exploreIdeaModel?.ideas = nil
                self.exploreIdeaModel = Mapper<ExploreIdeaModel>().map(JSONObject: responseData)
                
                
                
                if let dataModel = self.exploreIdeaModel {
                    
                    self.ideaBaseURL = dataModel.baseUrl ?? ""
                    if let ideaList = dataModel.ideas {
                        responseCount = ideaList.count
                        self.ideaItems.append(contentsOf: ideaList)
                    }
                     onSuccess(responseCount)
                 } else {
                    
                    onFailure(Helper.error(withLocalizedDescription: MacroConstant.INVALID_JSON_OBJECT))
                 }
                
            } else{
                onFailure(Helper.error(withLocalizedDescription: MacroConstant.INVALID_JSON_OBJECT))
            }
        }) { (error) in
            print(error?.localizedDescription as Any)
            onFailure(error)
        }
    }
}
