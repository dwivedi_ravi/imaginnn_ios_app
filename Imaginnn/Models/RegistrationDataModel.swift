//
//  RegistrationDataModel.swift
//  Imaginnn
//
//  Created by Bharat Khatke on 01/09/21.
//

import Foundation
import ObjectMapper

class RegistrationDataModel: BMKWrapper {
    
    var userName: String?
    var emailID: String?
    var mobileNo: String?
    var country: String?
    
    var registerUserModel: RegisterUserModel?
    var countryList = [String]()
    
    //MARK:- Get Country List here
    func getCountryDataURL(onSuccess: @escaping () -> Void, onFailure: @escaping (_ error: Error?) -> Void) {
        
        self.STATIC_GET(GET_COUNTRY_LIST, parameters: nil, success: { (response) in
            if let data = response as? NSArray  {
                if let array = data as? [String] {
                    self.countryList = array
                }
            }
            onSuccess()
        }) { (error) in
            onFailure(error)
        }
    }
    
    //MARK:- User Registration
    func registerUserDataURL(onSuccess: @escaping () -> Void, onFailure: @escaping (_ error: Error?) -> Void) {
        
        let errorLog = RegisterModel.validateUserDetails(userName ?? "", emailID ?? "", mobileNo ?? "")
        
        if let error = errorLog{
            onFailure(error)
        } else {
            let parameter = ["username" : userName ?? "", "email" : emailID ?? "", "mobileNumber" : "+91\(mobileNo ?? "")", "country" : country ?? "", "deviceId" : "", "appVersion" : APP_VERSION]
            print(parameter)
            
            self.unsignedPOST(POST_USER_REGISTRATION_URL, queryParams: nil, parameters: parameter, success: { (task, responseData) in
                print(responseData as Any)
                
                self.registerUserModel = Mapper<RegisterUserModel>().map(JSONObject: responseData)
                
                if !(self.registerUserModel?.result ?? false) {
                    let errorMsg = self.registerUserModel?.remark ?? ""
                    onFailure(Helper.error(withLocalizedDescription: errorMsg))
                } else {
                    self.saveRegisterUserDetails()
                    onSuccess()
                }
                
            }) { (task, error) in
                print(error?.localizedDescription as Any)
                onFailure(error)
            }
        }
    }
    
    private func saveRegisterUserDetails() {
        if let user = self.registerUserModel {
        
            let userId = user.userId ?? "0"
            let mobile_no = "+91\(mobileNo ?? "")"
            let userName = self.userName ?? ""
            let emailID = self.emailID ?? ""

            AppUtility.setValueWithKeyInKeyChain(value: userName, forkey: UserDetails.username.rawValue)
            AppUtility.setValueWithKeyInKeyChain(value: userId, forkey: UserDetails.userId.rawValue)
            AppUtility.setValueWithKeyInKeyChain(value: mobile_no, forkey: UserDetails.mobile_no.rawValue)
            AppUtility.setValueWithKeyInKeyChain(value: emailID, forkey: UserDetails.email.rawValue)
            
            ImaginnnAppManager.shared.email = emailID
            ImaginnnAppManager.shared.mobile_no = mobile_no
            ImaginnnAppManager.shared.userId = userId
            ImaginnnAppManager.shared.username = userName
            ImaginnnAppManager.shared.isUserLoged = false

        }
    }
    
}
