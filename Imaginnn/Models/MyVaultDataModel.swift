//
//  MyVaultDataModel.swift
//  Imaginnn
//
//  Created by Smruti Kanta Mohanty on 07/09/21.
//

import ObjectMapper

class MyVaultDataModel: BMKWrapper {
    
    var start_count: Int = 0
    var offset_count: Int = 10
    var filter: String?
    var exploreIdeaModel: ExploreIdeaModel?
    var ideaBaseURL: String?
    var myVaultItems = [IdeaModel]()
    
    
    
    //MARK:- Explore Data
    
    func postMyVaultIdeaDataURL(onSuccess: @escaping () -> Void, onFailure: @escaping (_ error: Error?) -> Void) {
        
        let userID = ImaginnnAppManager.shared.userId ?? ""
        let param = ["userId" : userID, "start_count" : start_count, "offset_count" : offset_count, "filter" : filter ?? "time"] as [String : Any]
       
        print(param)
        
       
        self.postData(POST_MYVAULT_IDEAS, parameters: param, success: { responseData in
            print(responseData as Any)
            
            if  responseData != nil  {
                self.exploreIdeaModel = nil
                self.exploreIdeaModel = Mapper<ExploreIdeaModel>().map(JSONObject: responseData)
                
                 if let dataModel = self.exploreIdeaModel {
                    self.ideaBaseURL = dataModel.baseUrl ?? ""
                    if let ideaList = dataModel.ideas {
                        self.myVaultItems.append(contentsOf: ideaList)
                    }
                     onSuccess()
                 } else {
                    
                    onFailure(Helper.error(withLocalizedDescription: MacroConstant.INVALID_JSON_OBJECT))
                 }
                
            } else{
                onFailure(Helper.error(withLocalizedDescription: MacroConstant.INVALID_JSON_OBJECT))
            }
        }) { (error) in
            print(error?.localizedDescription as Any)
            onFailure(error)
        }
    }
    
    //MARK:- Post Pick UP Address Details Data
    func uploadImageDataURL(paramDict: Any, image : UIImage, onSuccess: @escaping () -> Void, onFailure: @escaping (_ error: Error?) -> Void){
           
       
           let manager = AFHTTPSessionManager(baseURL: URL(string: BASE_URL))
           manager.requestSerializer = AFJSONRequestSerializer()
           manager.requestSerializer.timeoutInterval = 240
          
           let parameterDict = paramDict as! NSDictionary
           let fileName = parameterDict.value(forKey: "FileName")
           
        manager.post(POST_MYVAULT_IDEAS, parameters: paramDict, headers: nil, constructingBodyWith: { (data: AFMultipartFormData) in
    
               data.appendPart(withFileData: image.pngData()!, name: fileName as! String, fileName: "\(fileName ?? "").png", mimeType: "image/png")
               
           }, progress: nil, success: { (task: URLSessionDataTask?, responseData: Any?) in
               print("\(responseData ?? "")")
               onSuccess()
           }) { (task: URLSessionDataTask?, error: Error) in
               onFailure(error)
           }
    }
    
}
