//
//  PeopleDataModel.swift
//  Imaginnn
//
//  Created by Smruti Kanta Mohanty on 07/09/21.
//

import Foundation
import ObjectMapper

class PeopleDataModel : BMKWrapper {
    
    
    var friendListModel: FriendListModel?
    var userProfileURL: String?
    var friendList = [FriendModel]()
    
    func postPeolpleDataURL(onSuccess: @escaping () -> Void, onFailure: @escaping (_ error: Error?) -> Void) {
        
        let userID = ImaginnnAppManager.shared.userId ?? ""
       
        let param = ["userId" : userID]
       
        print(param)
        
       
        self.postData(POST_FRIEND_PEOPLE_LIST, parameters: param, success: { responseData in
            print(responseData as Any)
            
            if  responseData != nil  {
                self.friendListModel = nil
                self.friendListModel = Mapper<FriendListModel>().map(JSONObject: responseData)
                
                 if let dataModel = self.friendListModel {
                    self.userProfileURL = dataModel.userProfileURL ?? ""
                    if let friends = dataModel.friendList {
                        self.friendList.append(contentsOf: friends)
                    }
                     onSuccess()
                 } else {
                    
                    onFailure(Helper.error(withLocalizedDescription: MacroConstant.INVALID_JSON_OBJECT))
                 }
                
            } else{
                onFailure(Helper.error(withLocalizedDescription: MacroConstant.INVALID_JSON_OBJECT))
            }
        }) { (error) in
            print(error?.localizedDescription as Any)
            onFailure(error)
        }
    }
    
}


