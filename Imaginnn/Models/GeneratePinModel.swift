//
//  GeneratePinModel.swift
//  Imaginnn
//
//  Created by Bharat Khatke on 06/09/21.
//

import Foundation
import ObjectMapper

struct GeneratePinModel {
    static func validatePin(pin1: String, pin2: String, pin3: String, pin4: String) -> String? {
        if (!(pin1.isEmpty)  && !(pin2.isEmpty) && !(pin3.isEmpty) && !(pin4.isEmpty)) {
            let enteredPin = pin1 + pin2 + pin3 + pin4
            return enteredPin
        }
        return nil
    }
    
    
    static func isValidEnteredPinCode(enteredPin: String, reEnteredPin: String) -> String? {
        
        if enteredPin.isEmpty {
            return MacroConstant.INVALID_ENTERED_PIN
        } else if reEnteredPin.isEmpty {
            return MacroConstant.INVALID_RE_ENTERED_PIN
        } else if enteredPin != reEnteredPin {
            return MacroConstant.PIN_NOT_MATCHING_ERROR
        }
        return nil
    }
    
}

struct GeneratePINModel: Mappable {
    var code: String?
    var remarks: String?
    var userId: String?
    var result: Bool?
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        code <- map["code"]
        remarks <- map["remarks"]
        userId <- map["userId"]
        result <- map["result"]
    }
}

struct OtpCheckModel: Mappable{
    var code: String?
    var remarks: String?
    var userId: String?
    var token: String?
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        code <- map["code"]
        remarks <- map["remarks"]
        userId <- map["userId"]
        token <- map["token"]
    }
}
