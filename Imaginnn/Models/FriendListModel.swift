//
//  FriendListModel.swift
//  Imaginnn
//
//  Created by Bharat Khatke on 07/09/21.
//

import ObjectMapper

struct FriendListModel: Mappable {
    var userProfileURL: String?
    var userId: String?
    var friendList: [FriendModel]?
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        userProfileURL <- map["userProfileURL"]
        userId <- map["userId"]
        friendList <- map["friendList"]
    }
}

struct FriendModel: Mappable {
    var username: String?
    var userId: String?
    var profileImageName: String?
    var status: String?
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        username <- map["username"]
        userId <- map["userId"]
        profileImageName <- map["profileImageName"]
        status <- map["status"]
    }
}

