//
//  ExploreIdeaModel.swift
//  Imaginnn
//
//  Created by Smruti Kanta Mohanty on 07/09/21.
//

import Foundation
import ObjectMapper

struct ExploreIdeaModel: Mappable {
    
    
    var baseUrl: String?
    var userId: String?
    var ideas: [IdeaModel]?
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        baseUrl <- map["baseUrl"]
        userId <- map["userId"]
        ideas <- map["ideas"]
    }
}

struct IdeaModel: Mappable {
    var access_to: String?
    var author: String?
    var caption: String?
    var category: String?
    var description: String?
    var fav_count: Int?
    var ideaCreationDate: String?
    var ideaImage: String?
    var idea_id: Int?
    var isFavorite: Bool?
    var title: String?
    var userId: String?
    var views: String?
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        access_to <- map["access_to"]
        author <- map["author"]
        caption <- map["caption"]
        category <- map["category"]
        description <- map["description"]
        fav_count <- map["fav_count"]
        ideaCreationDate <- map["ideaCreationDate"]
        ideaImage <- map["ideaImage"]
        idea_id <- map["idea_id"]
        isFavorite <- map["isFavorite"]
        title <- map["title"]
        userId <- map["userId"]
        views <- map["views"]
    }
}



