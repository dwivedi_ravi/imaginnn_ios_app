//
//  LoginModel.swift
//  Imaginnn
//
//  Created by Smruti Kanta Mohanty on 31/08/21.
//

import Foundation
import ObjectMapper

struct CarouselData {
    var imageName: String    = ""
    var titleName: String    = ""
}

struct LoginModel {
  
    static func getCaroselData() -> [CarouselData]? {
        
        let item1 = CarouselData(imageName: "", titleName: "The Greteast glory in living lie not in never falling, but in rising every time we fall. \n- Nelson Mandela")
        
        let item2 = CarouselData(imageName: "", titleName: "Life is what happens when you're busy making other plans. \n- John Lennon")
        
        return [item1, item2]
    }
    
    static func validateUserLogin(_ userName: String, _ pinCode: String?) -> String? {
        
        if userName.isEmpty {
            return MacroConstant.INVALID_USER_NAME_ERROR
        } else if pinCode?.isEmpty ?? false {
            return MacroConstant.EMPTY_PIN_CODE_ERROR
        }
        
        return nil
    }
    
    static func validatePin(pin1: String, pin2: String, pin3: String, pin4: String) -> String? {
        if ((pin1.isEmpty)  || (pin2.isEmpty) || (pin3.isEmpty) || (pin4.isEmpty)) {
            return nil
        }else{
            let enteredPin = pin1 + pin2 + pin3 + pin4
            return enteredPin
        }
    }
    
    static func validateAlreadyLoggedUserPin(pin1: String, pin2: String, pin3: String, pin4: String, pin5: String, pin6: String) -> String? {
        if ((pin1.isEmpty)  || (pin2.isEmpty) || (pin3.isEmpty) || (pin4.isEmpty) || (pin5.isEmpty)  || (pin6.isEmpty)) {
            return nil
        }else{
            let enteredPin = pin1 + pin2 + pin3 + pin4 + pin5 + pin6
            return enteredPin
        }
    }
    
    
}

struct ValidUserModel: Mappable {
    var code: String?
    var remark: String?
    var result: Bool?
    var username: String?
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        code <- map["code"]
        remark <- map["remark"]
        result <- map["result"]
        username <- map["username"]
    }
}


struct LoginUser: Mappable {
    var mobile_no: String?
    var userId: String?
    var email: String?
    var username: String?
    var token: String?
    var code: String?
    var remark: String?
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        mobile_no <- map["mobile_no"]
        userId <- map["userId"]
        email <- map["email"]
        username <- map["username"]
        token <- map["token"]
        code <- map["code"]
        remark <- map["remark"]
    }
}
