//
//  FavoriteIdeasDataModel.swift
//  Imaginnn
//
//  Created by Smruti Kanta Mohanty on 07/09/21.
//

import ObjectMapper

class FavoriteIdeasDataModel: BMKWrapper {
    
    var start_count: Int = 0
    var offset_count: Int = 10
    var filter: String?
    var exploreIdeaModel: ExploreIdeaModel?
    var ideaBaseURL: String?
    var favoriteIdeaItems = [IdeaModel]()
    
    
    
    //MARK:- Explore Data
    
    func postFavoriteIdeaDataURL(onSuccess: @escaping (_ itemCount: Int) -> Void, onFailure: @escaping (_ error: Error?) -> Void) {
        
        let userID = ImaginnnAppManager.shared.userId ?? ""
        let param = ["userId" : userID, "start_count" : start_count, "offset_count" : offset_count, "filter" : filter ?? "time"] as [String : Any]
       
        print(param)
        
       
        self.postData(POST_FAVORITE_IDEAS, parameters: param, success: { responseData in
            print(responseData as Any)
            var itemCount = 0
            if  responseData != nil  {
                self.exploreIdeaModel = nil
                self.exploreIdeaModel = Mapper<ExploreIdeaModel>().map(JSONObject: responseData)
                
                 if let dataModel = self.exploreIdeaModel {
                    self.ideaBaseURL = dataModel.baseUrl ?? ""
                    if let ideaList = dataModel.ideas {
                        itemCount = ideaList.count
                        self.favoriteIdeaItems.append(contentsOf: ideaList)
                    }
                     onSuccess(itemCount)
                 } else {
                    
                    onFailure(Helper.error(withLocalizedDescription: MacroConstant.INVALID_JSON_OBJECT))
                 }
                
            } else{
                onFailure(Helper.error(withLocalizedDescription: MacroConstant.INVALID_JSON_OBJECT))
            }
        }) { (error) in
            print(error?.localizedDescription as Any)
            onFailure(error)
        }
    }
}
