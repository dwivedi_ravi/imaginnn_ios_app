//
//  AppUtilityModel.swift
//  Imaginnn
//
//  Created by Bharat Khatke on 31/08/21.
//

import UIKit

enum UserDetails: String {
    case mobile_no  = "mobile_no"
    case userId     = "userId"
    case email      = "email"
    case username   = "username"
    case token      = "token"
    case themeColor = "themeColor"
    case pin        = "pin"
}

class AppUtility {
    
    class func setValueWithKeyInKeyChain(value: String, forkey: String) {
        let keychainItemWrapper = KeychainItemWrapper(identifier: APP_IDENTIFIER, accessGroup: nil)
        keychainItemWrapper[forkey] = value as AnyObject
    }
    
    //MARK:- Get User Details
    class func getUserDetailsBy(key: UserDetails) -> String? {
        let keychainItemWrapper = KeychainItemWrapper(identifier: APP_IDENTIFIER, accessGroup: nil)
        
        switch key {
        
        case .mobile_no:
            if let mobile_no = keychainItemWrapper[UserDetails.mobile_no.rawValue] as? String {
                return mobile_no
            }
            return nil
            
        case .userId:
            if let userId = keychainItemWrapper[UserDetails.userId.rawValue] as? String {
                return userId
            }
            return nil
        
        case .email:
            if let email = keychainItemWrapper[UserDetails.email.rawValue] as? String {
                return email
            }
            return nil
            
        case .username:
            if let username = keychainItemWrapper[UserDetails.username.rawValue] as? String {
                return username
            }
            return nil
            
        case .token:
            if let token = keychainItemWrapper[UserDetails.token.rawValue] as? String {
                return token
            }
            return nil
            
        case .themeColor:
            if let themeColor = keychainItemWrapper[UserDetails.themeColor.rawValue] as? String {
                return themeColor
            }
            return nil
        case .pin:
            if let pin = keychainItemWrapper[UserDetails.pin.rawValue] as? String {
                return pin
            }
            return nil
        }
        
    }
}





