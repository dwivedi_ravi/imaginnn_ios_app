//
//  LoginDataModel.swift
//  Imaginnn
//
//  Created by Smruti Kanta Mohanty on 01/09/21.
//

import Foundation
import ObjectMapper

class LoginDataModel: BMKWrapper {
    
    var validUserData: ValidUserModel?
    var loginUser: LoginUser?
    var generatePinMode: GeneratePINModel?
    var otpCheckModel: OtpCheckModel?
    
    func checkValidUserDataURL(parameter: Any, onSuccess: @escaping () -> Void, onFailure: @escaping (_ error: Error?) -> Void) {
        
        self.unsignedPOST(POST_CHECK_USER_NAME_URL, queryParams: nil, parameters: parameter, success: { (task, responseData) in
            print(responseData)
           
            self.validUserData = Mapper<ValidUserModel>().map(JSONObject: responseData)
            onSuccess()

        }) { (task, error) in
            print(error?.localizedDescription as Any)
            onFailure(error)
        }
    }
    
    func loginUserDataURL(parameter: Any, onSuccess: @escaping () -> Void, onFailure: @escaping (_ error: Error?) -> Void) {
        
        self.unsignedPOST(POST_USER_LOGIN_URL, queryParams: nil, parameters: parameter, success: { (task, responseData) in
            print(responseData as Any)
           
            self.loginUser = Mapper<LoginUser>().map(JSONObject: responseData)
            
            if let error = self.isValidUser() {
                onFailure(error)
            } else {
                self.saveUserDetails()
                onSuccess()
            }
            
        }) { (task, error) in
            print(error?.localizedDescription as Any)
            onFailure(error)
        }
    }
    //POST_UPDATE_OTP_PIN
    func checkEnteredPINDataURL(onSuccess: @escaping () -> Void, onFailure: @escaping (_ error: Error?) -> Void) {
        
        let userID = ImaginnnAppManager.shared.userId ?? ""
        let mobileNumber = ImaginnnAppManager.shared.mobile_no ?? ""
        let email = ImaginnnAppManager.shared.email ?? ""
        
        
        let parameter = ["userId" : userID, "mobileNumber" : mobileNumber, "email" : email, "otpFlag" : "true"]
        self.unsignedPOST(POST_CHECK_OTP_PIN, queryParams: nil, parameters: parameter, success: { (task, responseData) in
            print(responseData as Any)
           
            
            self.otpCheckModel = Mapper<OtpCheckModel>().map(JSONObject: responseData)
            
            if let error = self.handleCheckPinResponse() {
                onFailure(error)
            } else {
                onSuccess()
            }
            
        }) { (task, error) in
            print(error?.localizedDescription as Any)
            onFailure(error)
        }
    }
    
    func updateEneteredPINDataURL(_ pinCode: String, onSuccess: @escaping () -> Void, onFailure: @escaping (_ error: Error?) -> Void) {
        
        let userID = ImaginnnAppManager.shared.userId ?? ""
        
        let parameter = ["userId" : userID, "pin" : pinCode, "action" : "update"]
        print("Update Pin Param: \(parameter)")

        self.postData(POST_UPDATE_OTP_PIN, parameters: parameter, success: { (response) in
            print(response as Any)
            onSuccess()
        }) { (error) in
            print(error?.localizedDescription as Any)
            onFailure(error)
        }
    
    }
    
    
    private func handleCheckPinResponse()  -> Error? {
        
        if let code = self.otpCheckModel?.code {
            let errorMessage = self.generatePinMode?.remarks ?? ""
            if code != "200" && code != "0" {
                return self.error(withMessage: errorMessage, errorCode: Int(code))
            } else {
                if let otpModel = self.otpCheckModel {
                    let authToken = otpModel.token ?? ""
                    AppUtility.setValueWithKeyInKeyChain(value: authToken, forkey: UserDetails.token.rawValue)
                    ImaginnnAppManager.shared.token = authToken
                }
            }
        }
        
        return nil
    }
    
    private func saveUserDetails() {
        if let user = self.loginUser {
            let username = user.username ?? ""
            let mobileNo = user.mobile_no ?? ""
            let token = user.token ?? ""
            let email = user.email ?? ""
            let userId = user.userId ?? "0"
            
            AppUtility.setValueWithKeyInKeyChain(value: username, forkey: UserDetails.username.rawValue)
            AppUtility.setValueWithKeyInKeyChain(value: mobileNo, forkey: UserDetails.mobile_no.rawValue)
            AppUtility.setValueWithKeyInKeyChain(value: token, forkey: UserDetails.token.rawValue)
            AppUtility.setValueWithKeyInKeyChain(value: email, forkey: UserDetails.email.rawValue )
            AppUtility.setValueWithKeyInKeyChain(value: userId, forkey: UserDetails.userId.rawValue)
            ImaginnnAppManager.shared.isUserLoged = true
            UpdateAppData.setSavedUserData()
        }
    }
    
    private func isValidUser() -> Error? {
        
        if let code = self.loginUser?.code {
            let errorMessage = self.loginUser?.remark ?? ""
            if code != "200" && code != "0" {
                return self.error(withMessage: errorMessage, errorCode: Int(code))
            }
        }
        return nil
    }
}


