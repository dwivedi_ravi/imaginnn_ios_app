//
//  ColorCollectionCell.swift
//  Imaginnn
//
//  Created by Smruti Kanta Mohanty on 19/10/21.
//

import Foundation

class ColorCollectionCell: UICollectionViewCell {
    @IBOutlet weak var colorView: BMKCustomView!
    @IBOutlet weak var colorViewHeightConstraint: NSLayoutConstraint!
    
    func setColorCellData(item: ColorItem) {
        
        if item.index == 1 {
            self.colorViewHeightConstraint.constant = 30
            self.colorView.cornerRadius = 15
        } else {
            self.colorViewHeightConstraint.constant = 20
            self.colorView.cornerRadius = 10
        }
        self.colorView.backgroundColor = item.color
    }
}


struct SignatureColor {
    
    static func getSignatureColorSet() -> [ColorItem] {
        
        let item1 = ColorItem(color: .black, index: 0, colorName: "black")
        let item2 = ColorItem(color: RED_COLOR, index: 0, colorName: "red")
        let item3 = ColorItem(color: YELLOW_COLOR, index: 0, colorName: "yellow")
        let item4 = ColorItem(color: GREEN_COLOR, index: 0, colorName: "green")
        let item5 = ColorItem(color: BLUE_COLOR, index: 0, colorName: "blue")
        let item6 = ColorItem(color: PURPLE_COLOR, index: 0, colorName: "purple")
        let item7 = ColorItem(color: .brown, index: 0, colorName: "brown")
        
        return [item1, item2, item3, item4, item5, item6, item7]
    }
}
