//
//  PeopleTableViewCell.swift
//  Imaginnn
//
//  Created by Smruti Kanta Mohanty on 07/09/21.
//

import UIKit

class PeopleTableViewCell: UITableViewCell {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var pleopleView: BMKCustomView!
    @IBOutlet weak var progressStatusImg: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imgView.layer.cornerRadius = 25
        imgView.layer.masksToBounds = true
    }
    
    func loadFriendDataOnCell(with cellData: FriendModel, baseURL: String)  {
        
        titleLbl.text = cellData.username ?? ""

        if cellData.status ?? "0" == "0" {
            progressStatusImg.isHidden = false
        } else {
            progressStatusImg.isHidden = true
        }
        
        if let imageName = cellData.profileImageName {
            let imageUrl = "\(baseURL)/\(imageName)"
            
            if let url = URL(string: imageUrl) {
                self.imgView.sd_setImage(with: url, placeholderImage: UIImage(named: "ic_profile_icon_small"), options: .highPriority, context: nil)
            }
        } else {
            
            let image = UIImage(named: "ic_profile_icon_small")
            if let themeColor: UIColor = ImaginnnAppManager.shared.selectedColor {
                self.imgView.image = image?.maskWithColor(color: themeColor)
            }
            
        }
    }
}
