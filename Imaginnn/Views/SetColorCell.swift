//
//  SetColorCell.swift
//  Imaginnn
//
//  Created by Smruti Kanta Mohanty on 31/08/21.
//

import Foundation


class SetColorCell: UICollectionViewCell {
    @IBOutlet weak var innerView: BMKCustomView!
    @IBOutlet weak var outerView: BMKCustomView!
}
