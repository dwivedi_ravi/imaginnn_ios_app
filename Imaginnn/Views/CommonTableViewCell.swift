//
//  CommonTableViewCell.swift
//  Imaginnn
//
//  Created by Smruti Kanta Mohanty on 02/09/21.
//

import UIKit
import SDWebImage

class CommonTableViewCell: UITableViewCell {
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var authorNameLbl: UILabel!
    @IBOutlet weak var durationLbl: UILabel!
    @IBOutlet weak var totalLikesLbl: UILabel!
    @IBOutlet weak var totalViewsLbl: UILabel!
    @IBOutlet weak var designationLbl: UILabel!
    
    @IBOutlet weak var privateIPRViewWidthConstraint: NSLayoutConstraint!//50
    @IBOutlet weak var likeNViewsWidthConstraint: NSLayoutConstraint! // 70
    
    override func awakeFromNib() {
        self.layoutIfNeeded()
        imgView.layer.cornerRadius = 10
        imgView.layer.masksToBounds = true
    }
    
    func loadExploreIdeaData(_ cellData: IdeaModel, baseUrl: String, _ isMyVaultCell: Bool = false) {
        self.titleLbl.text = cellData.title ?? ""
        self.descriptionLbl.text = cellData.caption ?? ""

        if let list = Helper.getSplittingStringArrayBy(cellData.category ?? "", ",") {
            if let catory = list.last, !catory.isEmpty {
                self.designationLbl.text = catory.uppercased()
            } else {
                self.designationLbl.text = list.first ?? ""
            }
        }
        
        if isMyVaultCell {
            self.likeNViewsWidthConstraint.constant = 0
            self.privateIPRViewWidthConstraint.constant = 0 //50
            
            if let access = cellData.access_to {
                if access.lowercased() != "unpublished" {
                    self.authorNameLbl.text = "Published - \(access)"
                    self.durationLbl.text   = "\(Helper.getNumberOfDays(dateStr: cellData.ideaCreationDate ?? "")) days ago"
                } else {
                    self.authorNameLbl.text = ""
                    self.durationLbl.text   = "Unpublished"
                }
            } else {
                 self.authorNameLbl.text = ""
                 self.durationLbl.text = "Unpublished"
            }
        } else {
            self.authorNameLbl.text  = "Author: \(cellData.author ?? "")"
            self.totalLikesLbl.text  = "Likes \(cellData.fav_count ?? 0)"
            self.totalViewsLbl.text  = "Views \(cellData.views ?? "0")"
            self.durationLbl.text    = "\(Helper.getNumberOfDays(dateStr: cellData.ideaCreationDate ?? "")) days ago"
        }

        if let imageName = cellData.ideaImage {
            let imageUrl = baseUrl + "/\(cellData.userId ?? "")" + "/\(cellData.idea_id ?? 0)" + "/\(imageName)"
            
            if let url = URL(string: imageUrl) {
                self.imgView.sd_setImage(with: url, placeholderImage: UIImage(named: "ic_placeholder_bg"), options: .highPriority, context: nil)
            }
        } else {
            self.imgView.image = UIImage(named: "ic_placeholder_bg")
        }
    }

}
