//
//  SelectFieldTableCell.swift
//  Imaginnn
//
//  Created by Bharat Khatke on 23/09/21.
//

import UIKit

class SelectFieldTableCell: UITableViewCell {

    @IBOutlet weak var fieldNameLbl: UILabel!
    @IBOutlet weak var checkBox: UIButton!
    typealias TapClosure = () -> Void
    var buttonTapped: TapClosure?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        checkBox.layer.cornerRadius = 8
        checkBox.layer.masksToBounds = true
    }

    func loadSelectFieldCell(cellData: CategoryModel) {
        
        fieldNameLbl.text = cellData.categoryName ?? ""
        
        if cellData.isSelected ?? false {
            checkBox.backgroundColor = ImaginnnAppManager.shared.selectedColor?.withAlphaComponent(1)
        } else {
            checkBox.backgroundColor = ImaginnnAppManager.shared.selectedColor?.withAlphaComponent(0.5)
        }
    }
    
    @IBAction func buttonTouchUpInside(sender: UIButton) {
        buttonTapped?()
    }
}
