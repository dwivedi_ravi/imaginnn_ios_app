//
//  GenericMapper.swift
//  Imaginnn
//
//  Created by Bharat Khatke on 01/09/21.
//

import Foundation
import ObjectMapper

class Result<T: Mappable>: Mappable {
    var result: T?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        result <- map["result"]
    }
}
