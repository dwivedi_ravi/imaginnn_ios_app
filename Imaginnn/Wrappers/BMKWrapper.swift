//
//  BMKWrapper.swift
//  Imaginnn
//
//  Created by Smruti Kanta Mohanty on 31/08/21.
//

import UIKit

let FinanceErrorDomain        = "ImagineErrorDomain"
let UNAUTHORIZED_STATUS_CODE  = 401
let RESPONSE_FAIL_ERROR_CODE  = 500


class BMKWrapper {
    
    func postData(_ urlString: String, parameters: Any?, success: @escaping (_ responseObject: Any?) -> Void, failure: @escaping (_ error: Error?) -> Void) {
        
        let manager = AFHTTPSessionManager(baseURL: URL(string: BASE_URL))
        manager.requestSerializer = AFJSONRequestSerializer()
        manager.requestSerializer.timeoutInterval = 600
        let userID = ImaginnnAppManager.shared.userId ?? ""
        let tokenStr = ImaginnnAppManager.shared.token ?? ""
        let token = "\(tokenStr):\(userID)"
        print(token)
        manager.requestSerializer.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        manager.requestSerializer.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        
        
        manager.post(urlString, parameters: parameters, headers: nil, progress: nil, success: { (task: URLSessionDataTask?, responseData: Any?) in
           // print(responseData as Any)
            if responseData != nil {
                if ((task?.response as? HTTPURLResponse)?.statusCode == 200) {
                    success(responseData)
                } else {
                    let err = self.error(withMessage: MacroConstant.COULD_NOT_CONTACT_SERVER, errorCode: 0)
                    failure(err)
                }
            } else {
                let err = self.error(withMessage: MacroConstant.INVALID_JSON_OBJECT, errorCode: 0)
                failure(err)
            }
        }) { (task: URLSessionDataTask?, error: Error) in
            let err = self.getProperError(error: error, task: task)
            failure(err)
        }
        
    }
    
    //MARK:- Post Without Auth key
    func postDataWithoutAuth(_ urlString: String, parameters: Any?, success: @escaping (_ responseObject: Any?) -> Void, failure: @escaping (_ error: Error?) -> Void) {
        
        let manager = AFHTTPSessionManager(baseURL: URL(string: BASE_URL))
        manager.requestSerializer = AFJSONRequestSerializer()
        manager.requestSerializer.timeoutInterval = 300
        manager.requestSerializer.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        
        
        manager.post(urlString, parameters: parameters, headers: nil, progress: nil, success: { (task: URLSessionDataTask?, responseData: Any?) in
            print(responseData as Any)
            if responseData != nil {
                if ((task?.response as? HTTPURLResponse)?.statusCode == 200) {
                    success(responseData)
                } else {
                    let err = self.error(withMessage: MacroConstant.COULD_NOT_CONTACT_SERVER, errorCode: 0)
                    failure(err)
                }
            } else {
                let err = self.error(withMessage: MacroConstant.INVALID_JSON_OBJECT, errorCode: 0)
                failure(err)
            }
        }) { (task: URLSessionDataTask?, error: Error) in
            let err = self.getProperError(error: error, task: task)
            failure(err)
        }
        
    }
    
    //MARK:- UNSIGNEDPOST Method
    func unsignedPOST(_ urlString: String, queryParams: [String : Any]?, parameters: Any?, success: @escaping (_ operation: URLSessionDataTask?, _ responseObject: Any?) -> Void, failure: @escaping (_ operation: URLSessionDataTask?, _ error: Error?) -> Void) {
        
        // Algorithm to build url string
        var components = URLComponents()
        components.path = urlString
        var queryItems: [AnyObject] = []
        
        if let data = queryParams, !data.isEmpty {
            for (key, _) in data {
                let keyString = key
                let value = data[key]
                
                queryItems.append(URLQueryItem(name: keyString,  value: value as? String) as AnyObject)
            }
            components.queryItems = queryItems as? [URLQueryItem]
        }
        
        
        let manager = AFHTTPSessionManager(baseURL: URL(string: BASE_URL))
        manager.requestSerializer = AFJSONRequestSerializer()
        
        manager.requestSerializer.timeoutInterval = 240
        manager.requestSerializer.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        manager.post(components.string!, parameters: parameters, headers: nil, progress: nil, success: { (task: URLSessionDataTask?, responseData: Any?) in
            if responseData != nil {
                
                if ((task?.response as? HTTPURLResponse)?.statusCode == 200) {
                    success(task,responseData)
                } else {
                    let err = self.error(withMessage: MacroConstant.COULD_NOT_CONTACT_SERVER, errorCode: 0)
                    failure(task, err)
                }
            } else {
                let err = self.error(withMessage: MacroConstant.INVALID_JSON_OBJECT, errorCode: 0)
                failure(task, err)
            }
        }) { (task: URLSessionDataTask?, error: Error) in
            let err = self.getProperError(error: error, task: task)
             failure(task, err)
        }
    }
    
    //MARK:- Use this to make Signed JSON(POST) call
    func signedPOST(_ urlString: String?, queryParams: [String : Any]?, parameters: Any?, success: @escaping (_ operation: URLSessionDataTask?, _ responseObject: Any?) -> Void, failure: @escaping (_ operation: URLSessionDataTask?, _ error: Error?) -> Void) {
        
        
        // Algorithm to build url string
        var components = URLComponents()
        components.path = urlString!
        var queryItems: [AnyObject] = []
        
        if let data = queryParams, !data.isEmpty {
            for (key, _) in data {
                let keyString = key
                let value = data[key]
                
                queryItems.append(URLQueryItem(name: keyString,  value: value as? String) as AnyObject)
            }
            
            components.queryItems = queryItems as? [URLQueryItem]
        }
        
        
        let manager = AFHTTPSessionManager(baseURL: URL(string: BASE_URL))
        manager.requestSerializer = AFJSONRequestSerializer()
        manager.requestSerializer.timeoutInterval = 240
        let token = AppUtility.getUserDetailsBy(key: .token) ?? ""
        
        manager.requestSerializer.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        manager.responseSerializer.acceptableContentTypes =  NSSet.init(array: ["application/json", "text/json", "text/javascript", "text/html", "text/plain", "application/x-www-form-urlencoded"]) as? Set<String>
        
        manager.post(components.string!, parameters: parameters, headers: nil, progress: nil, success: { (task: URLSessionDataTask?, responseData: Any?) in
            if responseData != nil {
                
                if ((task?.response as? HTTPURLResponse)?.statusCode == 200) {
                    success(task, responseData)
                } else {
                    let err = self.error(withMessage: MacroConstant.COULD_NOT_CONTACT_SERVER, errorCode: 0)
                    failure(task, err)
                }
            } else {
                let err = self.error(withMessage: MacroConstant.INVALID_JSON_OBJECT, errorCode: 0)
                failure(task, err)
            }
        }) { (task: URLSessionDataTask?, error: Error) in
            let err = self.getProperError(error: error, task: task)
            failure(task, err)
        }
    }
    
    func STATIC_GET(_ urlString: String, parameters: Any?, success: @escaping (_ responseObject: Any?) -> Void, failure: @escaping (_ error: Error?) -> Void) {
        
        let manager = AFHTTPSessionManager(baseURL: URL(string: BASE_URL))
        manager.requestSerializer = AFJSONRequestSerializer()
        manager.requestSerializer.timeoutInterval = 240
        manager.requestSerializer.setValue("Bearer \(STATIC_TOKEN)", forHTTPHeaderField: "Authorization")
       
        manager.requestSerializer.setValue("application/json;charset=UTF-8", forHTTPHeaderField: "Content-Type")
        
        manager.get(urlString, parameters: parameters, headers: nil, progress: nil, success: { (task: URLSessionDataTask?, responseData: Any?) in
            
            if responseData != nil {
                
                if ((task?.response as? HTTPURLResponse)?.statusCode == 200) {
                    success(responseData)
                } else {
                    let err = self.error(withMessage: MacroConstant.COULD_NOT_CONTACT_SERVER, errorCode: 0)
                    failure(err)
                }
            } else {
                let err = self.error(withMessage: MacroConstant.INVALID_JSON_OBJECT, errorCode: 0)
                failure(err)
            }
            
        }) { (dataTask: URLSessionDataTask?, error: Error) in
            failure(error)
        }
    }
    
    
    func GET(_ urlString: String, parameters: Any?, success: @escaping (_ responseObject: Any?) -> Void, failure: @escaping (_ error: Error?) -> Void) {
        
        let manager = AFHTTPSessionManager(baseURL: URL(string: BASE_URL))
        manager.requestSerializer = AFJSONRequestSerializer()
        manager.requestSerializer.timeoutInterval = 240
        let userID = ImaginnnAppManager.shared.userId ?? ""
        if let token = AppUtility.getUserDetailsBy(key: .token) {
            manager.requestSerializer.setValue("Bearer \(token):\(userID)", forHTTPHeaderField: "Authorization")
        }
       
        manager.requestSerializer.setValue("application/json", forHTTPHeaderField: "Content-Type")
        // manager.requestSerializer.setValue("application/json;charset=UTF-8", forHTTPHeaderField: "Content-Type")
        
        manager.get(urlString, parameters: parameters, headers: nil, progress: nil, success: { (task: URLSessionDataTask?, responseData: Any?) in
            
            if responseData != nil {
                
                if ((task?.response as? HTTPURLResponse)?.statusCode == 200) {
                    success(responseData)
                } else {
                    let err = self.error(withMessage: MacroConstant.COULD_NOT_CONTACT_SERVER, errorCode: 0)
                    failure(err)
                }
            } else {
                let err = self.error(withMessage: MacroConstant.INVALID_JSON_OBJECT, errorCode: 0)
                failure(err)
            }
            
        }) { (dataTask: URLSessionDataTask?, error: Error) in
            failure(error)
        }
    }
    
    //MARK:- UNSIGNEDGET Method
    func unsignedGET(_ urlString: String?, queryParams: [String : Any]?, parameters: Any?, success: @escaping (_ operation: URLSessionDataTask?, _ responseObject: Any?) -> Void, failure: @escaping (_ operation: URLSessionDataTask?, _ error: Error?) -> Void) {
        
        
        // Algorithm to build url string
        var components = URLComponents()
        components.path = urlString!
        var queryItems: [AnyObject] = []
        
        if let data = queryParams, !data.isEmpty {
            for (key, _) in data {
                let keyString = key
                let value = data[key]
                
                queryItems.append(URLQueryItem(name: keyString,  value: value as? String) as AnyObject)
            }
            
            components.queryItems = queryItems as? [URLQueryItem]
        }
        
        
        let manager = AFHTTPSessionManager(baseURL: URL(string: BASE_URL))
        manager.requestSerializer = AFJSONRequestSerializer()
        manager.requestSerializer.timeoutInterval = 240
        manager.requestSerializer.setValue("application/json;charset=UTF-8", forHTTPHeaderField: "Content-Type")
        
        manager.get(components.string!, parameters: parameters, headers: nil, progress: nil, success: { (task: URLSessionDataTask?, responseData: Any?) in
            if responseData != nil {
                
                if ((task?.response as? HTTPURLResponse)?.statusCode == 200) {
                    success(task, responseData)
                } else {
                    let err = self.error(withMessage: MacroConstant.COULD_NOT_CONTACT_SERVER, errorCode: 0)
                    failure(task, err)
                }
            } else {
                let err = self.error(withMessage: MacroConstant.INVALID_JSON_OBJECT, errorCode: 0)
                failure(task,err)
            }
        }) { (task: URLSessionDataTask?, error: Error) in
            let err = self.getProperError(error: error, task: task)
            failure(task, err)
        }
    }
    
    
    //MARK:- Error handling methods
    
    func error(withMessage: String, errorCode: Int?) -> NSError? {
        return NSError(domain: FinanceErrorDomain, code: errorCode ?? 0, userInfo: [NSLocalizedDescriptionKey:withMessage])
    }
    
    func getProperError(error: Error?, task: URLSessionDataTask?) -> Error? {
        
        if let httpResponse = task?.response as? HTTPURLResponse {
            let statucCode = httpResponse.statusCode
            if statucCode == UNAUTHORIZED_STATUS_CODE {
                return NSError(domain: FinanceErrorDomain, code: RESPONSE_FAIL_ERROR_CODE, userInfo: [NSLocalizedDescriptionKey: MacroConstant.COULD_NOT_CONTACT_SERVER])
                
            } else if statucCode == RESPONSE_FAIL_ERROR_CODE  {
                return NSError(domain: FinanceErrorDomain, code: RESPONSE_FAIL_ERROR_CODE, userInfo: [NSLocalizedDescriptionKey: MacroConstant.INVALID_JSON_OBJECT])
            }
            
            if statucCode == NSURLErrorUserCancelledAuthentication {
                return NSError(domain: FinanceErrorDomain, code: statucCode, userInfo: [NSLocalizedDescriptionKey: MacroConstant.INVAID_CREDENTIALS])
            }
            else if statucCode == NSURLErrorCancelled {
                return nil
            }
            else if statucCode == NSURLErrorUserCancelledAuthentication || statucCode == NSURLErrorTimedOut {
                return NSError(domain: FinanceErrorDomain, code: statucCode, userInfo: [NSLocalizedDescriptionKey: MacroConstant.COULD_NOT_CONTACT_SERVER])
            }
            else if statucCode == NSURLErrorNotConnectedToInternet {
                return NSError(domain: FinanceErrorDomain, code: statucCode, userInfo: [NSLocalizedDescriptionKey: MacroConstant.NO_INTERNET_CONNECTTION])
            }
            else if statucCode == NSURLErrorBadServerResponse {
                return NSError(domain: FinanceErrorDomain, code: statucCode, userInfo: [NSLocalizedDescriptionKey: MacroConstant.INVALID_JSON_OBJECT])
            }else {
                return self.error(withMessage: error?.localizedDescription ?? "", errorCode: statucCode)
            }
        }
        return self.error(withMessage: error?.localizedDescription ?? "", errorCode: error?.code ?? 0)
    }
    
}

extension Error {
    var code: Int { return (self as NSError).code }
    var domain: String { return (self as NSError).domain }
}
