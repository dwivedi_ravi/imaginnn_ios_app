//
//  Constant.swift
//  Imaginnn
//
//  Created by Bharat Khatke on 29/08/21.
//

import UIKit


let FORGOT_BTN_TAG = 101
let RESEND_BTN_TAG = 102

let EMAIL_ID_MAX_LENGTH      = 50
let MOBILE_NO_MAX_LENGTH     = 10


//MARK:- API URL's

let BASE_URL = "http://imaginnn.com:443"


let GET_COUNTRY_LIST = "/ImagineApp/countries/list"
let POST_USER_REGISTRATION_URL = "/ImagineApp/userReg"
let POST_CHECK_USER_NAME_URL   = "/ImagineApp/username"
let POST_USER_LOGIN_URL        = "/ImagineApp/userLogin"
let POST_CHECK_OTP_PIN         = "/ImagineApp/OtpCheck"
let POST_UPDATE_OTP_PIN        = "/ImagineApp/pinUpdate"
let POST_EXPLORE_IDEAS         = "/ImagineApp/showIdeas"
let POST_MYVAULT_IDEAS         = "/ImagineApp/showIdeasForUser"
let POST_FAVORITE_IDEAS        = "/ImagineApp/getAllfavorite"
let POST_FRIEND_PEOPLE_LIST    = "/ImagineApp/friends/list"
let GET_CATEGORY_LIST          = "/ImagineApp/displayIdeaCategory"
let POST_CREATE_IDEA_URL       = "/ImagineApp/createIdea"

let APP_IDENTIFIER = "imaginnn"

let STATIC_TOKEN = "58f56ff4-53b5-4768-b3d3-b29f94391765:10"
let APP_VERSION  = Helper.getAppBundleVersion()
var DEVICE_TOKEN = ""
//MARK:- Basic Colors in Apps
let RED_COLOR = Helper.hexStringToUIColor(hex: "#EB5757")
let BLUE_COLOR = Helper.hexStringToUIColor(hex: "#2F80ED")
let ORANGE_COLOR = Helper.hexStringToUIColor(hex: "#F2994A")
let NAVI_BLUE_COLOR = Helper.hexStringToUIColor(hex: "#2D9CDB")
let YELLOW_COLOR = Helper.hexStringToUIColor(hex: "#F2C94C")
let SKY_BLUE_COLOR = Helper.hexStringToUIColor(hex: "#56CCF2")
let GREEN_COLOR = Helper.hexStringToUIColor(hex: "#219653")
let PURPLE_COLOR = Helper.hexStringToUIColor(hex: "#9B51E0")

let COMMON_COLOR = Helper.hexStringToUIColor(hex: "#F2F2F2")
let ALERT_VIEW_COLOR = Helper.hexStringToUIColor(hex: "#F0F0F0")



