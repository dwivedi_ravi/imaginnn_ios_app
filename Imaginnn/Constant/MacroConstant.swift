//
//  MacroConstant.swift
//  Imaginnn
//
//  Created by Bharat Khatke on 01/09/21.
//

import Foundation


struct MacroConstant {
    static let INVAID_CREDENTIALS        = "Credentials are invalid"
    static let COULD_NOT_CONTACT_SERVER  = "Could not contact server."
    static let NO_INTERNET_CONNECTTION   = "Could not reach out for Internet.\nPlease try again."
    static let INVALID_JSON_OBJECT       = "Server sent improper data"
    static let INVALID_USER_NAME_ERROR   = "Please enter Username or Email Id"
    static let EMPTY_PIN_CODE_ERROR      = "Please enter 4 digit PIN"
    static let EMPTY_USER_NAME_ERROR     = "Username should not be empty"
    static let MAINDATORY_FIELDS_ERROR   = "Mandatory fields are missing"
    static let EMPTY_EMAIL_ERROR         = "Email Id should not be empty"
    static let EMPTY_MOBILE_NO_ERROR     = "Mobile no should not be empty"
    static let INVALID_EMAIL_ID_ERROR    = "Invalid Email Id"
    static let INVALID_ENTERED_PIN       = "Entered PIN is empty"
    static let INVALID_RE_ENTERED_PIN    = "Re entered PIN is empty"
    static let PIN_NOT_MATCHING_ERROR    = "Entered PIN and confirmed PIN not matching"
    static let PLEASE_ENTER_VALID_PIN    = "Please Enter valid PIN"
    
    static let SHOW_PIN_GENERATION_MSG   = "Your mobile number is verified please enter your new 4 digit pin"
    static let MAX_CATEGORY_ITEM_MSG     = "You can select maximum 3 fields"
    
    //Add Create ID Message
    //static let CREATE_IDEA_TITLE_ERROR   = "Please enter title"
    
    static func eneterEmptyFieldMessage(_ fieldName: String) -> String {
        return "Please enter \(fieldName)"
    }
    
}
